# Try to find the profiler librarie and include files
# profiler is part of the Google performance tools suite
# http://code.google.com/p/google-perftools/
#
# PROFILER_INCLUDE_DIR where to find profiler.h, etc.
# PROFILER_LIBRARIES libraries to link against
# PROFILER_FOUND If false, do not try to use PROFILER

find_path ( PROFILER_INCLUDE_DIR profiler.h
            PATHS /usr/local/include/google
)
find_path ( PROFILER_LIBRARY_DIR libprofiler.so
            PATHS /usr/local/lib
)

find_library ( PROFILER_LIB NAMES profiler )
set ( PROFILER_LIBRARIES ${PROFILER_LIB} )

# handle the QUIETLY and REQUIRED arguments and set PROFILER_FOUND to TRUE if
# all listed variables are TRUE
include ( FindPackageHandleStandardArgs )
find_package_handle_standard_args( Profiler DEFAULT_MSG PROFILER_LIBRARIES PROFILER_INCLUDE_DIR PROFILER_LIBRARY_DIR )

mark_as_advanced ( PROFILER_INCLUDE_DIR PROFILER_LIB PROFILER_LIBRARIES PROFILER_FOUND )


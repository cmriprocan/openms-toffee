/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#include "RTNormalisation.h"

#include <tbb/parallel_reduce.h>

#include <openms-toffee/io_openms.h>
#include <openms-toffee/OpenMSToffeeWorkflow_impl/TargetedExperimentOps.h>
#include <openms-toffee/OpenMSToffeeWorkflow_impl/ToffeeScorer.h>

#include <OpenMS/FORMAT/TransformationXMLFile.h>
#include <OpenMS/TRANSFORMATIONS/FEATUREFINDER/ElutionModelFitter.h>

namespace openmstoffee {
RTNormalisation::RTNormalisation(
    const std::string& toffeeFilePath,
    const std::string& alignmentTSVFilePath
) :
    mToffeeFilePath(toffeeFilePath),
    mAlignmentTSVFilePath(alignmentTSVFilePath),
    mNormalisationParams(RTNormalisation::defaultNormalisationParams()),
    mChromExtractParams(ChromatogramExtractor::defaultParams(true)),
    mFeatureFinderParams(ToffeeScorer::defaultFeatureFinderParams(true)),
    mMzCorrectionFunction(RTNormalisation::defaultMzCorrectionFunction()),
    mMinRSquared(RTNormalisation::defaultMinRSquared()),
    mMinCoverage(RTNormalisation::defaultMinCoverage()) {}

void
RTNormalisation::updateNormalisationParams(
    const OpenMS::Param &param
) {
    mNormalisationParams = param;
}

void
RTNormalisation::updateChromExtractParams(
    const OpenMS::ChromExtractParams &param
) {
    mChromExtractParams = param;
}

void
RTNormalisation::updateFeatureFinderParams(
    const OpenMS::Param &param
) {
    mFeatureFinderParams = param;
}

void
RTNormalisation::updateMzCorrectionFunction(
    const std::string &mzCorrectionFunction
) {
    mMzCorrectionFunction = mzCorrectionFunction;
}

void
RTNormalisation::updateMinRSquared(
    double minRSquared
) {
    mMinRSquared = minRSquared;
}

void
RTNormalisation::updateMinCoverage(
    double minCoverage
) {
    mMinCoverage = minCoverage;
}

OpenMS::Param
RTNormalisation::defaultNormalisationParams()
{
    OpenMS::Param p;

    p.setValue("alignmentMethod", "linear", "How to perform the alignment to the normalized RT space using anchor points. 'linear': perform linear regression (for few anchor points). 'interpolated': Interpolate between anchor points (for few, noise-free anchor points). 'lowess' Use local regression (for many, noisy anchor points). 'b_spline' use b splines for smoothing.");
    p.setValidStrings("alignmentMethod", OpenMS::ListUtils::create<OpenMS::String>("linear,interpolated,lowess,b_spline"));
    p.setValue("lowess:span", 2.0/3, "Span parameter for lowess");
    p.setMinFloat("lowess:span", 0.0);
    p.setMaxFloat("lowess:span", 1.0);
    p.setValue("b_spline:num_nodes", 5, "Number of nodes for b spline");
    p.setMinInt("b_spline:num_nodes", 0);

    p.setValue("outlierMethod", "iter_residual", "Which outlier detection method to use (valid: 'iter_residual', 'iter_jackknife', 'ransac', 'none'). Iterative methods remove one outlier at a time. Jackknife approach optimizes for maximum r-squared improvement while 'iter_residual' removes the datapoint with the largest residual error (removal by residual is computationally cheaper, use this with lots of peptides).");
    p.setValidStrings("outlierMethod", OpenMS::ListUtils::create<OpenMS::String>("iter_residual,iter_jackknife,ransac,none"));

    p.setValue("useIterativeChauvenet", "false", "Whether to use Chauvenet's criterion when using iterative methods. This should be used if the algorithm removes too many datapoints but it may lead to true outliers being retained.");
    p.setValidStrings("useIterativeChauvenet", OpenMS::ListUtils::create<OpenMS::String>("true,false"));

    p.setValue("RANSACMaxIterations", 1000, "Maximum iterations for the RANSAC outlier detection algorithm.");
    p.setValue("RANSACMaxPercentRTThreshold", 3, "Maximum threshold in RT dimension for the RANSAC outlier detection algorithm (in percent of the total gradient). Default is set to 3% which is around +/- 4 minutes on a 120 gradient.");
    p.setValue("RANSACSamplingSize", 10, "Sampling size of data points per iteration for the RANSAC outlier detection algorithm.");

    p.setValue("estimateBestPeptides", "false", "Whether the algorithms should try to choose the best peptides based on their peak shape for normalization. Use this option you do not expect all your peptides to be detected in a sample and too many 'bad' peptides enter the outlier removal step (e.g. due to them being endogenous peptides or using a less curated list of peptides).");
    p.setValidStrings("estimateBestPeptides", OpenMS::ListUtils::create<OpenMS::String>("true,false"));

    p.setValue("InitialQualityCutoff", 0.5, "The initial overall quality cutoff for a peak to be scored (range ca. -2 to 2)");
    p.setValue("OverallQualityCutoff", 5.5, "The overall quality cutoff for a peak to go into the retention time estimation (range ca. 0 to 10)");
    p.setValue("NrRTBins", 10, "Number of RT bins to use to compute coverage. This option should be used to ensure that there is a complete coverage of the RT space (this should detect cases where only a part of the RT gradient is actually covered by normalization peptides)");
    p.setValue("MinPeptidesPerBin", 1, "Minimal number of peptides that are required for a bin to counted as 'covered'");
    p.setValue("MinBinsFilled", 8, "Minimal number of bins required to be covered");

    return p;
}

OpenMS::TransformationDescription
RTNormalisation::calculateNormalisation() const
{
    throw_assert(
        mMzCorrectionFunction == "none",
        "We cannot currently handle any correction functions: " << mMzCorrectionFunction
    );
    auto alignmentTransitions = TargetedExperimentOps::loadLightTSV(mAlignmentTSVFilePath);
    auto chromatograms = loadChromatograms(
            alignmentTransitions
    );

    // load the swath maps -- these can be empty unless we want to adjust m/z
    // while calculating the normalisation
    std::vector<OpenSwath::SwathMap> osSwathMaps;

    // ---
    // perform RT and m/z correction on the data
    OpenMS::OpenSwathRetentionTimeNormalization wf;
    return wf.RTNormalization(
            alignmentTransitions,
            chromatograms,
            mMinRSquared,
            mMinCoverage,
            mFeatureFinderParams,
            mNormalisationParams,
            osSwathMaps,
            mMzCorrectionFunction,
            mChromExtractParams.mz_extraction_window,
            mChromExtractParams.ppm
    );
}

OpenMS::TransformationDescription
RTNormalisation::calculateNormalisationFromFile(
    const std::string &inputTrafoPath
) const
{
    OpenMS::TransformationDescription worldToIRTTransformation;
    bool fitModel = true;
    OpenMS::TransformationXMLFile().load(
        inputTrafoPath,
        worldToIRTTransformation,
        fitModel
    );
    return worldToIRTTransformation;
}

std::vector<OpenMS::MSChromatogram>
RTNormalisation::loadChromatograms(
        const OpenSwath::LightTargetedExperiment & alignmentTransitions
) const
{
    const toffee::SwathRun swathRun(mToffeeFilePath);
    auto windows = swathRun.ms2Windows();

    // ---
    ChromatogramExtractor chromExtractor(mChromExtractParams);
    OpenMS::TransformationDescription nullIrtTransformation;
    std::vector<OpenMS::MSChromatogram> chromatograms;
    for (const auto& window : windows) {
        auto inWindowTargetedExperiment = TargetedExperimentOps::filterToMassRange(
            alignmentTransitions,
            window.lower,
            window.upper,
            mChromExtractParams.min_upper_edge_dist
        );

        const auto& transitions = inWindowTargetedExperiment.getTransitions();
        if (transitions.empty()) {
            continue;
        }

        auto peptideMap = TargetedExperimentOps::mapPeptidesToIds(inWindowTargetedExperiment);

        auto swathMap = swathRun.loadSwathMapPtr(window.name);
        for (const auto& transition : transitions) {
            const auto& peptidePtr = peptideMap.at(transition.getPeptideRef());
            throw_assert(
                peptidePtr,
                "Failed to find the peptide for: " << transition.getPeptideRef()
            );
            auto chromatogram = chromExtractor.extractChromatogram(
                *swathMap,
                *peptidePtr,
                transition,
                nullIrtTransformation
            );
            chromatograms.emplace_back(chromatogram);
        }
    }

    std::sort(
            chromatograms.begin(),
            chromatograms.end(),
            [](const auto& a, const auto& b) -> bool { return a.getMZ() < b.getMZ(); }
    );
    return chromatograms;
}

void
RTNormalisation::saveToFile(
    const OpenMS::TransformationDescription &transform,
    const std::string& trafoXMLFilePath
) const
{
    OpenMS::TransformationXMLFile().store(trafoXMLFilePath, transform);
}

void
RTNormalisation::calculateAndSaveToFile(
        const std::string& trafoXMLFilePath
) const
{
    auto transform = calculateNormalisation();
    saveToFile(transform, trafoXMLFilePath);
}
}

/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <algorithm>
#include <iostream>
#include <fstream>
#include <cmath>
#include <OpenMS/FORMAT/SwathFile.h>
#include <OpenMS/DATASTRUCTURES/String.h>
#include <OpenMS/ANALYSIS/OPENSWATH/SwathWindowLoader.h>
#include <OpenMS/OPENSWATHALGO/DATAACCESS/ISpectrumAccess.h>
#include <OpenMS/KERNEL/MSExperiment.h>
#include <toffee/io.h>

namespace openmstoffee {

template<typename StreamT>
StreamT& operator<<(
        StreamT& os,
        typename OpenMS::PeakMap::SpectrumType& spectrum
)
{
    os << "Spectrum(";
    os << spectrum.getName();
    os << ", RT = " << spectrum.getRT();
    os << ", MSLevel=" << spectrum.getMSLevel();
    os << ", size=" << spectrum.size();
    os << ")";
    return os;
}

template<typename StreamT>
StreamT& operator<<(
        StreamT& os,
        typename OpenMS::PeakMap::ChromatogramType& chromatogram
)
{
    os << "Chromatogram(";
    os << chromatogram.getName();
    os << ", m/z = " << chromatogram.getMZ();
    os << ", size=" << chromatogram.size();
    os << ")";
    return os;
}

template<typename StreamT>
StreamT& operator<<(
        StreamT& os,
        typename OpenSwath::Spectrum& spectrum
)
{
    os << "Spectrum(";
    const auto& mzArray = spectrum.getMZArray();
    os << "m/z size = " << (mzArray ? mzArray->data.size() : -1);
    const auto& intensityArray = spectrum.getIntensityArray();
    os << ", intensity size = " << (intensityArray ? intensityArray->data.size() : -1);
    os << ")";
    return os;
}

template<typename StreamT>
StreamT& operator<<(
        StreamT& os,
        typename OpenSwath::Chromatogram& chromatogram
)
{
    os << "Chromatogram(";
    const auto& timeArray = chromatogram.getTimeArray();
    os << "time size = " << (timeArray ? timeArray->data.size() : -1);
    const auto& intensityArray = chromatogram.getIntensityArray();
    os << ", intensity size = " << (intensityArray ? intensityArray->data.size() : -1);
    os << ")";
    return os;
}

template<typename StreamT>
StreamT& operator<<(
        StreamT& os,
        typename OpenSwath::ISpectrumAccess& spectrumAccess
)
{
    os << "SpectrumAccess";

    const auto& numSpectra = spectrumAccess.getNrSpectra();
    os << toffee::endl << "\t      Number of spectra = " << numSpectra;
    for (size_t i = 0; i < numSpectra; ++i) {
        const auto& s = spectrumAccess.getSpectrumById(i);
        os << toffee::endl << "Spectra " << i << " :: ";
        if (s) {
            os << *s;
        } else {
            os << "null";
        }
    }

    const auto& numChromatograms = spectrumAccess.getNrChromatograms();
    os << toffee::endl << "\tNumber of chromatograms = " << numChromatograms;
    for (size_t i = 0; i < numChromatograms; ++i) {
        const auto& c = spectrumAccess.getChromatogramById(i);
        os << toffee::endl << "Chromatogram " << i << " :: ";
        if (c) {
            os << *c;
        } else {
            os << "null";
        }
    }

    return os;
}

template<typename StreamT>
StreamT& operator<<(
        StreamT& os,
        const typename OpenSwath::SwathMap& map
)
{
    os << "SwathMap";
    os << toffee::endl << "\t lower = " << map.lower;
    os << toffee::endl << "\tcenter = " << map.center;
    os << toffee::endl << "\t upper = " << map.upper;
    os << toffee::endl << "\t   ms1 = " << std::boolalpha << map.ms1;
    const auto& spectrumAccess = map.sptr;
    os << toffee::endl;
    if (spectrumAccess) {
        os << *spectrumAccess;
    } else {
        os << "Spectrum: null";
    }
    return os;
}
}
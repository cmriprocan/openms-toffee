/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
/*
 * An OpenMS style TOPP application for the OpenMS-Toffee workflow.
 */

#pragma once
#include <OpenMS/APPLICATIONS/TOPPBase.h>
#include <OpenMS/DATASTRUCTURES/String.h>
#include <OpenMS/DATASTRUCTURES/ListUtils.h>
#include <OpenMS/ANALYSIS/OPENSWATH/OpenSwathWorkflow.h>

namespace openmstoffee {
class OpenMSToffeeWorkflow
    :
        public OpenMS::TOPPBase {
public:
    enum class FileFormat {
        TSV,
        SQLITE
    };

    struct FileArguments {
        std::string toffeeFilePath; ///< input tof file path
        std::string srlFilePath; ///< input srl file path (tsv or pqp)
        std::string alignmentTSVFilePath; ///< input alightment tsv file path
        std::string outputFilePath; ///< output file path
        std::string inputRTTrafoXML; ///< if not empty, use this to specify RT norm (trafoXML)
        std::string outputRTTrafoXML; ///< if not empty, save RT norm to here (trafoXML)
        FileFormat format; ///< defines if using TSV or SQLite
    };

    explicit OpenMSToffeeWorkflow(bool testing = false);

protected:
    void registerOptionsAndFlags_() override;
    OpenMS::Param getSubsectionDefaults_(const OpenMS::String &name) const override;
    ExitCodes main_(int argc, const char **argv) override;

private:
    /*
     * If testMode is true, then (would you believe it?) we're testing.
     * Currently this means that the actual analysis doesn't run, just
     * the argument processing.
     */
    bool mTestMode;

    /*
     * =================================================================
     * Parameter handling support
     * =================================================================
     */
    void registerChromExtractOptions();
    FileArguments getFileArguments();
    OpenMS::ChromExtractParams getChromExtractParams(
        bool forRTNormalisation
    );
    OpenMS::Param getFeatureFinderParams(
        bool useMS1
    );
    OpenMS::Param getNormalisationParams();
};
}

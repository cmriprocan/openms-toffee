/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#include "OpenMSToffeeWorkflow.h"
#include <openms-toffee/io_openms.h>
#include <openms-toffee/OpenMSToffeeWorkflow_impl/ToffeeScorer.h>
#include <openms-toffee/OpenMSToffeeWorkflow_impl/ChromatogramExtractor.h>
#include <openms-toffee/RTNormalisation.h>
#include <openms-toffee/OpenMSToffeeWorkflow_impl/ToffeeScorer.h>
#include <OpenMS/ANALYSIS/OPENSWATH/TransitionTSVFile.h>
#include <OpenMS/FORMAT/FileHandler.h>

using namespace OpenMS;

namespace openmstoffee {
OpenMSToffeeWorkflow::OpenMSToffeeWorkflow(bool testing)
    :
    TOPPBase(
        "OpenMSToffeeWorkflow",
        "Complete workflow to run the ProCan Toffee analysis tools.",
        false
    ),
    mTestMode(testing) {
}


/*
 * =================================================================
 * TOPPBase's requirements
 * =================================================================
 */
void
OpenMSToffeeWorkflow::registerOptionsAndFlags_() {

    registerInputFile_("in", "<file>", "", "Input file");              // Only one file just now
    setValidFormats_("in", ListUtils::create<String>("tof"), false);   // Only toffee files allowed

    registerInputFile_("tr", "<file>", "", "transition file ('tsv','pqp')");
    setValidFormats_("tr", ListUtils::create<String>("tsv,pqp"));      // Removed traML format

    registerStringOption_(
        "tr_type",
        "<type>",
        "",
        "input file type -- default: determined from file extension or content\n",
        false
    );
    setValidStrings_("tr_type", ListUtils::create<String>("tsv,pqp")); // Removed traML format

    registerInputFile_("tr_irt", "<file>", "", "transition file ('tsv')", false);
    setValidFormats_("tr_irt", ListUtils::create<String>("tsv"));

    registerFlag_("use_ms1_traces", "Extract the precursor ion trace(s) and use for scoring", true);

    registerOutputFile_("out", "<file>", "", "Output file", false);
    setValidFormats_("out", ListUtils::create<String>("tsv,osw"));

    registerInputFile_(
        "rt_norm",
        "<file>",
        "",
        "RT normalization file (how to map the RTs of this run to the ones stored in the library). If set, tr_irt may be omitted.",
        false,
        true
    );
    setValidFormats_("rt_norm", ListUtils::create<String>("trafoXML"));

    registerChromExtractOptions();

    registerDoubleOption_("min_rsq", "<double>", RTNormalisation::defaultMinRSquared(), "Minimum r-squared of RT peptides regression", false, true);
    registerDoubleOption_(
        "min_coverage", "<double>", RTNormalisation::defaultMinCoverage(), "Minimum relative amount of RT peptides to keep", false,
        true
    );

    registerStringOption_(
        "mz_correction_function",
        "<name>",
        RTNormalisation::defaultMzCorrectionFunction(),
        "Use the retention time normalization peptide MS2 masses to perform a mass correction (linear, weighted by intensity linear or quadratic) of all spectra.",
        false,
        true
    );
    setValidStrings_(
        "mz_correction_function", ListUtils::create<String>(
            "none,unweighted_regression,weighted_regression,quadratic_regression,weighted_quadratic_regression,weighted_quadratic_regression_delta_ppm,quadratic_regression_delta_ppm"
        ));

    registerSubsection_("Scoring", "Scoring parameters section");
    registerSubsection_(
        "RTNormalization",
        "Parameters for the RTNormalization for iRT petides. This specifies how the RT alignment is performed and how outlier detection is applied. Outlier detection can be done iteratively (by default) which removes one outlier per iteration or using the RANSAC algorithm."
    );
    registerSubsection_("Debugging", "Debugging");
}

Param
OpenMSToffeeWorkflow::getSubsectionDefaults_(const String &name) const {
    if (name == "Scoring") {
        return ToffeeScorer::defaultFeatureFinderParams(false);
    }
    else if (name == "RTNormalization") {
        return RTNormalisation::defaultNormalisationParams();
    }
    else if (name == "Debugging") {
        Param p;
        p.setValue("irt_trafo", "", "Transformation file for RT transform");
        return p;
    }
    else {
        throw Exception::InvalidValue(__FILE__, __LINE__, OPENMS_PRETTY_FUNCTION, "Unknown subsection", name);
    }
}

namespace {
template<typename OpenSwathWriter>
TOPPBase::ExitCodes
mainImpl(
    OpenMSToffeeWorkflow::FileArguments fileArgs,
    const RTNormalisation& rtNormalisation,
    ToffeeScorer scorer
) {
    OpenMS::ProgressLogger logger;
    logger.setLogType(OpenMS::ProgressLogger::LogType::CMD);
    logger.startProgress(0, 1, "Calculate scores");

    // ---
    // clean up the output file
    std::remove(fileArgs.outputFilePath.c_str());
    if (fileArgs.format == OpenMSToffeeWorkflow::FileFormat::SQLITE) {
        // copy the PQP file into the OSW file
        std::ifstream src(fileArgs.srlFilePath.c_str(), std::ios::binary);
        std::ofstream dst(fileArgs.outputFilePath.c_str(), std::ios::binary);
        dst << src.rdbuf();
        dst.close();
        src.close();
    }

    // ---
    // final initialisation
    OpenMS::TransformationDescription worldToIRTTransformation;
    if (fileArgs.inputRTTrafoXML.empty()) {
        worldToIRTTransformation = rtNormalisation.calculateNormalisation();
    }
    else {
        worldToIRTTransformation = rtNormalisation.calculateNormalisationFromFile(
            fileArgs.inputRTTrafoXML
        );
    }
    if (!fileArgs.outputRTTrafoXML.empty()) {
        rtNormalisation.saveToFile(worldToIRTTransformation, fileArgs.outputRTTrafoXML);
    }
    scorer.setWorldToIRTTransformation(
        worldToIRTTransformation
    );

    // load the SRL
    OpenSwath::LightTargetedExperiment targetedExperiment;
    if (fileArgs.format == OpenMSToffeeWorkflow::FileFormat::SQLITE) {
        targetedExperiment = TargetedExperimentOps::loadLightPQP(fileArgs.srlFilePath);
    }
    else {
        targetedExperiment = TargetedExperimentOps::loadLightTSV(fileArgs.srlFilePath);
    }
    scorer.setTargetedExperiment(targetedExperiment);

    // ---
    // set up the output writer
    OpenSwathWriter writer(
        fileArgs.outputFilePath,
        fileArgs.toffeeFilePath,
        scorer.useMS1(),
        /*sonar=*/false,
        /*uis_scores=*/false
    ); // only active if filename not empty
    throw_assert(writer.isActive(), "Failed to create writer");
    writer.writeHeader();

    // ---
    // score each of the discoveries
    auto swathRun = scorer.swathRun();
    throw_assert(swathRun, "Swath data hasn't been loaded yet!");
    auto windows = swathRun->ms2Windows();
    for (const auto &window : windows) {
        auto scores = scorer.calculateScores(window);
        std::vector<OpenMS::String> lines;
        for (auto& score : scores) {
            lines.push_back(writer.prepareLine(
                score.peptide,
                score.transitions.data(),
                score.features,
                score.id
            ));
        }
        writer.writeLines(lines);
    }

    logger.endProgress();
    return TOPPBase::ExitCodes::EXECUTION_OK;
}
}

TOPPBase::ExitCodes
OpenMSToffeeWorkflow::main_(int argc, const char **argv) {
    // ---
    // configure file inputs
    FileArguments fileArgs;
    try {
        fileArgs = getFileArguments();
    }
    catch (ExitCodes flag) {
        return flag;
    }

    // ---
    // configure retention time
    RTNormalisation rtNormalisation(
        fileArgs.toffeeFilePath,
        fileArgs.alignmentTSVFilePath
    );
    try {
        rtNormalisation.updateNormalisationParams(
            getNormalisationParams()
        );
        rtNormalisation.updateChromExtractParams(
            getChromExtractParams(true)
        );
        rtNormalisation.updateMzCorrectionFunction(
            getStringOption_("mz_correction_function")
        );
        rtNormalisation.updateMinRSquared(
            getDoubleOption_("min_rsq")
        );
        rtNormalisation.updateMinCoverage(
            getDoubleOption_("min_coverage")
        );
    }
    catch (ExitCodes flag) {
        return flag;
    }

    // ---
    // configure the scorer
    ToffeeScorer scorer;
    try {
        auto useMS1 = getFlag_("use_ms1_traces");
        scorer.setToffeeFilePath(fileArgs.toffeeFilePath, useMS1);
        scorer.updateChromExtractParams(getChromExtractParams(false));
        scorer.updateFeatureFinderParams(getFeatureFinderParams(useMS1));
    }
    catch (ExitCodes flag) {
        return flag;
    }

    // ---
    // run the analysis
    if (!mTestMode) {
        switch (fileArgs.format) {
            case FileFormat::SQLITE:
                return mainImpl<OpenMS::OpenSwathOSWWriter>(
                    fileArgs,
                    rtNormalisation,
                    scorer
                );
            case FileFormat::TSV:
                return mainImpl<OpenMS::OpenSwathTSVWriter>(
                    fileArgs,
                    rtNormalisation,
                    scorer
                );
        }
        throw_assert(false, "Shouldn't reach here!");
    }
    return TOPPBase::ExitCodes::EXECUTION_OK;
}

/*
 * =================================================================
 * Parameter handling
 * =================================================================
 */

/*
 * Extract the file related arguments from the command line - the
 * input file, the output file, &c.
 */
OpenMSToffeeWorkflow::FileArguments
OpenMSToffeeWorkflow::getFileArguments() {

    OpenMSToffeeWorkflow::FileArguments fileArgs;

    // The main input (toffee) file
    // ----------------------------
    fileArgs.toffeeFilePath = getStringOption_("in");

    // The SRL and its format
    // ----------------------
    String tr_file = getStringOption_("tr");
    FileTypes::Type tr_type = FileTypes::nameToType(getStringOption_("tr_type"));

    if (tr_type == FileTypes::UNKNOWN) {
        tr_type = FileHandler::getType(tr_file);
        writeDebug_(String("Input file type (-tr): ") + FileTypes::typeToName(tr_type), 2);
    }

    if (tr_type == FileTypes::UNKNOWN) {
        writeLog_("Error: Could not determine input file type for '-tr' !");
        throw PARSE_ERROR;
    }

    fileArgs.srlFilePath = tr_file;

    // Now set the Format flag based on the input file specified.
    if (tr_type == FileTypes::TSV) {
        fileArgs.format = OpenMSToffeeWorkflow::FileFormat::TSV;
    }
    else if (tr_type == FileTypes::PQP) {
        fileArgs.format = OpenMSToffeeWorkflow::FileFormat::SQLITE;
    }
    else {
        writeLog_(String("Error: Unsupported file type for '-tr': ") + FileTypes::typeToName(tr_type));
        throw INPUT_FILE_NOT_READABLE;
    }

    // The alignment file (aka the tr_irt file in OpenSWATH speak).
    // ------------------------------------------------------------
    fileArgs.alignmentTSVFilePath = getStringOption_("tr_irt");

    // The output file.
    // ----------------
    /*
     * The required format of the output file is determined by the input file.
     *      TSV in => TSV out
     *      PQP in => OSW out.
     */
    fileArgs.outputFilePath = getStringOption_("out");
    FileTypes::Type out_type = FileHandler::getType(fileArgs.outputFilePath);
    writeDebug_(String("Output file type (-out): ") + FileTypes::typeToName(out_type), 2);

    if (out_type == FileTypes::UNKNOWN) {
        writeLog_("Error: Could not determine input file type for '-out' !");
        throw PARSE_ERROR;
    }

    // Now check that the specified output file is correct for the input file.
    if (fileArgs.format == OpenMSToffeeWorkflow::FileFormat::TSV) {
        if (out_type != FileTypes::TSV) {
            writeLog_(
                String("Error: Input of type TSV requires an output of type TSV (")
                    + FileTypes::typeToName(out_type) + " specified)!"
            );
            throw PARSE_ERROR;
        }
    }
    else if (fileArgs.format == OpenMSToffeeWorkflow::FileFormat::SQLITE) {
        if (out_type != FileTypes::OSW) {
            writeLog_(
                String("Error: Input of type PQP requires an output of type OSW (")
                    + FileTypes::typeToName(out_type) + " specified)!"
            );
            throw PARSE_ERROR;
        }
    }
    else {
        writeLog_("Internal Error: unexpected input type");
        throw INTERNAL_ERROR;
    }

    // ---
    // trafoXML file
    auto trafoParam = getParam_().copy("Debugging:", true);
    fileArgs.outputRTTrafoXML = trafoParam.getValue("irt_trafo").toString();
    fileArgs.inputRTTrafoXML = getStringOption_("rt_norm");

    if (fileArgs.alignmentTSVFilePath.empty() && fileArgs.inputRTTrafoXML.empty()) {
        writeLog_("You must specify either one of irt_trafo or rt_norm!");
        throw ILLEGAL_PARAMETERS;
    }
    if (!fileArgs.alignmentTSVFilePath.empty() && !fileArgs.inputRTTrafoXML.empty()) {
        writeLog_("You must specify exactly one of irt_trafo or rt_norm, not both!");
        throw ILLEGAL_PARAMETERS;
    }

    return fileArgs;
}

/*
 * Get the Chromatagram Extraction related arguments from the command line
 */
OpenMS::ChromExtractParams
OpenMSToffeeWorkflow::getChromExtractParams(
    bool forRTNormalisation
) {
    auto result = ChromatogramExtractor::defaultParams(forRTNormalisation);

    String massUnitName("mass_unit");
    String massValueName("mz_extraction_window");
    if (forRTNormalisation) {
        massUnitName = String("irt_") + massUnitName;
        massValueName = String("irt_") + massValueName;
    }
    else {
        // only for non-RT config
        result.extraction_function = getStringOption_("extraction_function");
        result.rt_extraction_window = getDoubleOption_("rt_extraction_window");
        result.extra_rt_extract = getDoubleOption_("extra_rt_extraction_window");
        result.min_upper_edge_dist = getDoubleOption_("min_upper_edge_dist");
    }

    result.mz_extraction_window = getDoubleOption_(massValueName);
    result.ppm = getStringOption_(massUnitName) == String("ppm");

    return result;
}

/*
 * Get the scoring related arguments as modified by the command line
 */
OpenMS::Param
OpenMSToffeeWorkflow::getFeatureFinderParams(
    bool useMS1
) {
    auto param = getParam_().copy("Scoring:", true);
    if (useMS1) {
        param.setValue("Scores:use_ms1_correlation", "true");
        param.setValue("Scores:use_ms1_fullscan", "true");
    }
    return param;
}

OpenMS::Param
OpenMSToffeeWorkflow::getNormalisationParams() {
    return getParam_().copy("RTNormalization:", true);
}

void
OpenMSToffeeWorkflow::registerChromExtractOptions() {
    auto defaults = ChromatogramExtractor::defaultParams(false);
    auto irtDefaults = ChromatogramExtractor::defaultParams(true);

    bool isAdvanced = true;

    registerDoubleOption_(
        "min_upper_edge_dist",
        "<double>",
        defaults.min_upper_edge_dist,
        "Minimal distance to the edge to still consider a precursor, in Thomson",
        false,
        isAdvanced
    );

    registerDoubleOption_(
        "rt_extraction_window",
        "<double>",
        defaults.rt_extraction_window,
        "Only extract RT around this value (-1 means extract over the whole range, a value of 600 means to extract around +/- 300 s of the expected elution).",
        false,
        !isAdvanced
    );

    registerDoubleOption_(
        "mz_extraction_window",
        "<double>",
        defaults.mz_extraction_window,
        "Extraction window used (in Thomson, to use ppm see -ppm flag)",
        false,
        !isAdvanced
    );
    setMinFloat_("mz_extraction_window", 0.0);

    registerStringOption_(
        "mass_unit",
        "<name>",
        defaults.ppm ? "ppm" : "thomson",
        "Unit used to describe the mz_extraction_window",
        false,
        !isAdvanced
    );
    setValidStrings_(
        "mass_unit",
        ListUtils::create<String>("ppm,thomson")
    );

    registerDoubleOption_(
        "irt_mz_extraction_window",
        "<double>",
        irtDefaults.mz_extraction_window,
        "Extraction window used (in Thomson, to use ppm see -ppm flag)",
        false,
        isAdvanced
    );
    setMinFloat_("irt_mz_extraction_window", 0.0);

    registerStringOption_(
        "irt_mass_unit",
        "<name>",
        irtDefaults.ppm ? "ppm" : "thomson",
        "Unit used to describe the irt_mz_extraction_window",
        false,
        isAdvanced
    );
    setValidStrings_(
        "irt_mass_unit",
        ListUtils::create<String>("ppm,thomson")
    );

    registerStringOption_(
        "extraction_function",
        "<name>",
        defaults.extraction_function,
        "Function used to extract the signal",
        false,
        isAdvanced
    );
    setValidStrings_(
        "extraction_function",
        ListUtils::create<String>("tophat,bartlett")
    );

    registerDoubleOption_(
        "extra_rt_extraction_window",
        "<double>",
        defaults.extra_rt_extract,
        "Output an XIC with a RT-window that is by this much larger (e.g. to visually inspect a larger area of the chromatogram)",
        false,
        isAdvanced
    );
}

}

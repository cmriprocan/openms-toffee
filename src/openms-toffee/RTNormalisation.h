/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <string>
#include <vector>

#include <toffee/SwathRun.h>
#include <toffee/SwathMap.h>

#include <OpenMS/ANALYSIS/MAPMATCHING/TransformationDescription.h>
#include <OpenMS/ANALYSIS/TARGETED/TargetedExperiment.h>
#include <OpenMS/ANALYSIS/OPENSWATH/OpenSwathWorkflow.h>

namespace openmstoffee {

/// Calculate the world to iRT normalisation transformation using raw
/// SWATH-MS data contained in a toffee file, and a list of precursor and
/// product ions that can be used for alignment
class RTNormalisation {
public:

    /// @param toffeeFilePath the toffee file for which we wish to calculate
    /// the world to iRT normalisation
    /// @param alignmentTSVFilePath the path to a TSV file of retention time
    /// alignment precursor and product ions
    RTNormalisation(
        const std::string& toffeeFilePath,
        const std::string& alignmentTSVFilePath
    );

    /// Default parameters to input into the normalisation algorithms. These can
    /// be updated, see below.
    static OpenMS::Param
    defaultNormalisationParams();

    /// Update the default parameters to input into the normalisation algorithms.
    void
    updateNormalisationParams(
        const OpenMS::Param &param
    );

    /// Default method for correcting the mass over charge. This can
    /// be updated, see below.
    static std::string
    defaultMzCorrectionFunction() { return "none"; }

    /// Update the default method for correcting the mass over charge
    void
    updateMzCorrectionFunction(
        const std::string &mzCorrectionFunction
    );

    /// Default minimum R-squared value in regression fitting method. This can
    /// be updated, see below.
    static double
    defaultMinRSquared() { return 0.95; }

    /// Update the default minimum R-squared value in regression fitting method.
    void
    updateMinRSquared(
        double minRSquared
    );

    /// Default minimum coverage of the fit regression method. This can
    /// be updated, see below.
    static double
    defaultMinCoverage() { return 0.6; }

    /// Update the default minimum coverage of the fit regression method.
    void
    updateMinCoverage(
        double minCoverage
    );

    /// Update the chromatogram extraction configuration.
    void
    updateChromExtractParams(
        const OpenMS::ChromExtractParams &param
    );

    /// Update the feature finding configuration.
    void
    updateFeatureFinderParams(
        const OpenMS::Param &param
    );

    /// Calculate the world to iRT normalisation
    OpenMS::TransformationDescription
    calculateNormalisation() const;

    /// Calculate the world to iRT normalisation
    OpenMS::TransformationDescription
    calculateNormalisationFromFile(
        const std::string &inputTrafoPath
    ) const;

    /// Save a previously calculated iRT normalisation to file.
    ///
    /// @warning this function does not check if the transformation is
    /// in world to iRT coordinates, or its inverse. It relies on the
    /// user to take care!
    void
    saveToFile(
        const OpenMS::TransformationDescription &transform,
        const std::string& trafoXMLFilePath
    ) const;

    /// Calculate the world to iRT normalisation and save it to file
    void
    calculateAndSaveToFile(
        const std::string& trafoXMLFilePath
    ) const;

private:
    std::string mToffeeFilePath;
    std::string mAlignmentTSVFilePath;
    OpenMS::Param mNormalisationParams;
    OpenMS::ChromExtractParams mChromExtractParams;
    OpenMS::Param mFeatureFinderParams;
    std::string mMzCorrectionFunction;
    double mMinRSquared;
    double mMinCoverage;

    std::vector<OpenMS::MSChromatogram>
    loadChromatograms(
            const OpenSwath::LightTargetedExperiment & alignmentTransitions
    ) const;
};

}

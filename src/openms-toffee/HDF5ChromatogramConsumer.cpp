/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include "HDF5ChromatogramConsumer.h"
#include "openms-toffee/io_openms.h"
#include <H5Cpp.h>
#include <OpenMS/FORMAT/MzMLFile.h>
#include <boost/filesystem.hpp>

namespace openmstoffee {
void
HDF5ChromatogramConsumer::chromatogramToHDF5(
    const std::string &mzMLFilePath,
    const std::string &h5FilePath
) {
    throw_assert(
        boost::filesystem::exists(mzMLFilePath),
        "The mzML file does not exist: " + mzMLFilePath
    );
    HDF5ChromatogramConsumer consumer(h5FilePath);
    OpenMS::MzMLFile().transform(
        mzMLFilePath,
        &consumer,
        /*skip_full_count=*/false,
        /*skip_first_pass=*/true
    );
}

HDF5ChromatogramConsumer::HDF5ChromatogramConsumer(
    const std::string &h5FilePath
) :
    mHDFPath(h5FilePath),
    mChromatogramNames(),
    mDataOffset(),
    mDataSize(),
    mRetentionTime(),
    mIntensity() {
    // if the file exists, overwrite it -- otherwise, create a new file
    H5::H5File h5File(mHDFPath, H5F_ACC_TRUNC);
    h5File.close();
}

HDF5ChromatogramConsumer::~HDF5ChromatogramConsumer() {
    LOG("Creating file");
    H5::H5File h5File(mHDFPath, H5F_ACC_RDWR);

    // write the names
    {
        // HDF5 only understands vector of char*
        std::vector<const char *> cStrings;
        std::transform(
            mChromatogramNames.cbegin(),
            mChromatogramNames.cend(),
            std::back_inserter(cStrings),
            [](const auto &s) -> const char * { return s.c_str(); }
        );

        static constexpr size_t RANK = 1;
        hsize_t dims[RANK] = {cStrings.size()};
        H5::DataSpace dataspace(RANK, dims);

        H5::DSetCreatPropList plist;
        plist.setChunk(RANK, dims);
        plist.setDeflate(6);

        // Variable length string
        H5::StrType datatype(H5::PredType::C_S1, H5T_VARIABLE);
        h5File.createDataSet("names", datatype, dataspace, plist).write(cStrings.data(), datatype);
    }

    // write the offsets
    {
        static constexpr size_t RANK = 1;
        hsize_t dims[RANK] = {mDataOffset.size()};
        H5::DataSpace dataspace(RANK, dims);

        H5::DSetCreatPropList plist;
        plist.setChunk(RANK, dims);
        plist.setDeflate(6);

        auto datatype = H5::PredType::NATIVE_UINT64;
        h5File.createDataSet("offset", datatype, dataspace, plist).write(mDataOffset.data(), datatype);
    }

    // write the sizes
    {
        static constexpr size_t RANK = 1;
        hsize_t dims[RANK] = {mDataOffset.size()};
        H5::DataSpace dataspace(RANK, dims);

        H5::DSetCreatPropList plist;
        plist.setChunk(RANK, dims);
        plist.setDeflate(6);

        auto datatype = H5::PredType::NATIVE_UINT32;
        h5File.createDataSet("size", datatype, dataspace, plist).write(mDataSize.data(), datatype);
    }

    // write the intensity
    {
        static constexpr size_t RANK = 1;
        hsize_t dims[RANK] = {mIntensity.size()};
        H5::DataSpace dataspace(RANK, dims);

        H5::DSetCreatPropList plist;
        plist.setChunk(RANK, dims);
        plist.setDeflate(6);

        auto datatype = H5::PredType::NATIVE_UINT32;
        h5File.createDataSet("intensity", datatype, dataspace, plist).write(mIntensity.data(), datatype);
    }

    // write the retention time
    {
        static constexpr size_t RANK = 1;
        hsize_t dims[RANK] = {mRetentionTime.size()};
        H5::DataSpace dataspace(RANK, dims);

        H5::DSetCreatPropList plist;
        plist.setChunk(RANK, dims);
        plist.setDeflate(6);

        auto datatype = H5::PredType::NATIVE_DOUBLE;
        h5File.createDataSet("retentionTime", datatype, dataspace, plist).write(mRetentionTime.data(), datatype);
    }

    h5File.close();
}

void
HDF5ChromatogramConsumer::consumeSpectrum(
    SpectrumType &s
) {
}

void
HDF5ChromatogramConsumer::consumeChromatogram(
    ChromatogramType &c
) {

    mChromatogramNames.emplace_back(c.getNativeID());
    mDataOffset.push_back(mRetentionTime.size());
    mDataSize.push_back(c.size());

    for (const auto &peak : c) {
        mRetentionTime.push_back(peak.getRT());
        mIntensity.push_back(peak.getIntensity());
    }

    if (mChromatogramNames.size() % 100000 == 1) {
        LOG(mChromatogramNames.size() << "|" << mChromatogramNames.back() << ": " << c);
    }
}

void
HDF5ChromatogramConsumer::setExpectedSize(
    size_t expectedSpectra,
    size_t expectedChromatograms
) {
}

void
HDF5ChromatogramConsumer::setExperimentalSettings(
    const OpenMS::ExperimentalSettings &exp
) {
}

}

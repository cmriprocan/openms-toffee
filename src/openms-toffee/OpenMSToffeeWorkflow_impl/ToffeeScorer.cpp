/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#include "ToffeeScorer.h"

#include <tbb/parallel_reduce.h>
#include <tbb/blocked_range.h>

#include <openms-toffee/io_openms.h>
#include <openms-toffee/OpenMSToffeeWorkflow_impl/ChromatogramExtractor.h>
#include <openms-toffee/OpenMSToffeeWorkflow_impl/TransitionGroupScorer.h>
#include <openms-toffee/OpenMSToffeeWorkflow_impl/TargetedExperimentOps.h>
#include <openms-toffee/OpenMSToffeeWorkflow_impl/ToffeeSpectrumAccess.h>

#include <OpenMS/ANALYSIS/OPENSWATH/ChromatogramExtractor.h>
#include <OpenMS/ANALYSIS/OPENSWATH/DATAACCESS/SpectrumAccessOpenMS.h>
#include <OpenMS/ANALYSIS/OPENSWATH/OpenSwathWorkflow.h>
#include <toffee/ToffeeWriter.h>

namespace openmstoffee {
ToffeeScorer::ToffeeScorer() :
    mToffeeFilePath(),
    mSwathRun(nullptr),
    mMS1SwathMap(nullptr),
    mMS1SwathMapSpectrumAccess(nullptr),
    mChromExtractParams(ChromatogramExtractor::defaultParams(false)),
    mFeatureFinderParams(ToffeeScorer::defaultFeatureFinderParams(false)),
    mWorldToIRTTransformation(),
    mTargetedExperiment() {}

void
ToffeeScorer::setToffeeFilePath(
    const std::string& toffeeFilePath,
    bool loadMS1
) {
    mToffeeFilePath = toffeeFilePath;
    mSwathRun = std::make_shared<const toffee::SwathRun>(toffeeFilePath);
    if (loadMS1) {
        mMS1SwathMap = mSwathRun->loadSwathMapPtr(
            toffee::ToffeeWriter::MS1_NAME
        );
        mMS1SwathMapSpectrumAccess = mSwathRun->loadSwathMapSpectrumAccessPtr(
            toffee::ToffeeWriter::MS1_NAME
        );
    }
}

void
ToffeeScorer::updateChromExtractParams(
    const OpenMS::ChromExtractParams &param
) {
    mChromExtractParams = param;
}

void
ToffeeScorer::updateFeatureFinderParams(
    const OpenMS::Param &param
) {
    mFeatureFinderParams = param;
}

void
ToffeeScorer::setWorldToIRTTransformation(
    const OpenMS::TransformationDescription& worldToIRTTransformation
) {
    mWorldToIRTTransformation = worldToIRTTransformation;
    mIrtToWorldTransformation = worldToIRTTransformation;
    mIrtToWorldTransformation.invert();
}

void
ToffeeScorer::setTargetedExperiment(
    const OpenSwath::LightTargetedExperiment& targetedExperiment
) {
    mTargetedExperiment = targetedExperiment;
}

OpenMS::Param
ToffeeScorer::defaultFeatureFinderParams(
    bool forRTNormalisation
) {
    OpenMS::Param feature_finder_param = OpenMS::MRMFeatureFinderScoring().getDefaults();
    feature_finder_param.remove("rt_extraction_window");
    feature_finder_param.setValue(
        "rt_normalization_factor",
        100.0
    ); // for iRT peptides between 0 and 100 (more or less)

    feature_finder_param.setValue("TransitionGroupPicker:min_peak_width", 14.0);
    feature_finder_param.setValue("TransitionGroupPicker:recalculate_peaks", "true");
    feature_finder_param.setValue("TransitionGroupPicker:compute_peak_quality", "true");
    feature_finder_param.setValue("TransitionGroupPicker:minimal_quality", -1.5);
    feature_finder_param.setValue("TransitionGroupPicker:background_subtraction", "none");
    feature_finder_param.remove("TransitionGroupPicker:stop_after_intensity_ratio");

    // Peak Picker
    feature_finder_param.setValue("TransitionGroupPicker:PeakPickerMRM:use_gauss", "false");
    feature_finder_param.setValue("TransitionGroupPicker:PeakPickerMRM:sgolay_polynomial_order", 3);
    feature_finder_param.setValue("TransitionGroupPicker:PeakPickerMRM:sgolay_frame_length", 11);
    feature_finder_param.setValue("TransitionGroupPicker:PeakPickerMRM:peak_width", -1.0);
    feature_finder_param.setValue("TransitionGroupPicker:PeakPickerMRM:remove_overlapping_peaks", "true");
    feature_finder_param.setValue(
        "TransitionGroupPicker:PeakPickerMRM:write_sn_log_messages",
        "false"
    ); // no log messages
    // TODO it seems that the legacy method produces slightly larger peaks, e.g. it will not cut off peaks too early
    // however the same can be achieved by using a relatively low SN cutoff in the -Scoring:TransitionGroupPicker:PeakPickerMRM:signal_to_noise 0.5
    feature_finder_param.setValue("TransitionGroupPicker:recalculate_peaks_max_z", 0.75);
    feature_finder_param.setValue("TransitionGroupPicker:PeakPickerMRM:method", "corrected");
    feature_finder_param.setValue("TransitionGroupPicker:PeakPickerMRM:signal_to_noise", 0.1);
    feature_finder_param.setValue("TransitionGroupPicker:PeakPickerMRM:gauss_width", 30.0);
    feature_finder_param.setValue("uis_threshold_sn", 0);
    feature_finder_param.setValue("uis_threshold_peak_area", 0);
    feature_finder_param.remove("TransitionGroupPicker:PeakPickerMRM:sn_win_len");
    feature_finder_param.remove("TransitionGroupPicker:PeakPickerMRM:sn_bin_count");
    feature_finder_param.remove("TransitionGroupPicker:PeakPickerMRM:stop_after_feature");

    // EMG Scoring - turn off by default since it is very CPU-intensive
    feature_finder_param.setValue("Scores:use_elution_model_score", "false");
    feature_finder_param.setValue("EMGScoring:max_iteration", 10);
    feature_finder_param.remove("EMGScoring:interpolation_step");
    feature_finder_param.remove("EMGScoring:tolerance_stdev_bounding_box");
    feature_finder_param.remove("EMGScoring:deltaAbsError");

    // remove these parameters
    feature_finder_param.remove("add_up_spectra");
    feature_finder_param.remove("spacing_for_spectra_resampling");
    feature_finder_param.remove("EMGScoring:statistics:mean");
    feature_finder_param.remove("EMGScoring:statistics:variance");

    if (forRTNormalisation) {
        feature_finder_param.setValue("Scores:use_rt_score", "false");
        feature_finder_param.setValue("rt_extraction_window", -1.0);
        feature_finder_param.setValue("TransitionGroupPicker:PeakPickerMRM:signal_to_noise", 1.0); // set to 1.0 in all cases
        feature_finder_param.setValue("TransitionGroupPicker:compute_peak_quality", "false"); // no peak quality -> take all peaks!
    }

    return feature_finder_param;
}

namespace {
class ToffeeScorerImpl {
public:
    using PeptideTransitionPair = std::pair<OpenSwath::LightCompound, std::vector<OpenSwath::LightTransition> >;
    using InputVecType = std::vector<PeptideTransitionPair>;
    using RangeType = tbb::blocked_range<InputVecType::const_iterator>;

    using ResultType = std::vector<TransitionGroupScorer::TransitionGroupScores>;

    explicit ToffeeScorerImpl(
        TransitionGroupScorer scorer
    ) :
        mResult(),
        mScorer(scorer) {}

    ToffeeScorerImpl(
        ToffeeScorerImpl& other,
        tbb::split
    ) :
        mResult(),
        mScorer(other.mScorer) {}

    void
    join(
        const ToffeeScorerImpl& other
    ) {
        mResult.insert(
            mResult.end(),
            other.mResult.begin(),
            other.mResult.end()
        );
    }

    ResultType
    sortedScores() const
    {
        auto scores = mResult;
        std::sort(
            scores.begin(),
            scores.end(),
            [](const auto& a, const auto& b) -> bool {
                return (a.peptide.id < b.peptide.id) ||
                    ((a.peptide.id == b.peptide.id)
                        && (a.features.begin()->getIntensity() < b.features.begin()->getIntensity()));
            }
        );
        return scores;
    }

    void
    operator()(
        const RangeType& peptideTransitions
    )
    {
        for (const auto& peptideTransitionPair : peptideTransitions) {
            auto scores = mScorer.calculate(
                peptideTransitionPair.first,
                peptideTransitionPair.second
            );
            if (!scores.features.empty()) {
                mResult.emplace_back(scores);
            }
        }
    }

private:
    ResultType mResult;
    TransitionGroupScorer mScorer;
};
}

std::vector<TransitionGroupScorer::TransitionGroupScores>
ToffeeScorer::calculateScores(
        const toffee::MS2WindowDescriptor& window
) const {
    throw_assert(
        !mToffeeFilePath.empty(),
        "You need to have set the toffee file name"
    );
    throw_assert(
        !mTargetedExperiment.getTransitions().empty(),
        "No transitions found, did you set the targeted experiment?"
    );

    // ---
    // open the transitions for this window
    auto inWindowTargetedExperiment = TargetedExperimentOps::filterToMassRange(
        mTargetedExperiment,
        window.lower,
        window.upper,
        mChromExtractParams.min_upper_edge_dist
    );

    // map peptides to their transitions
    auto peptideMap = TargetedExperimentOps::mapPeptidesToIds(inWindowTargetedExperiment);
    if (peptideMap.empty()) {
        return std::vector<TransitionGroupScorer::TransitionGroupScores>();
    }
    auto transitionMap = TargetedExperimentOps::mapTransitionsToPeptideIds(inWindowTargetedExperiment);
    if (transitionMap.empty()) {
        return std::vector<TransitionGroupScorer::TransitionGroupScores>();
    }

    // ---
    // set up the swath maps
    std::vector<OpenSwath::SwathMap> osSwathMaps;
    if (mMS1SwathMapSpectrumAccess) {
        osSwathMaps.emplace_back(ToffeeSpectrumAccess::toOpenSwathMap(mMS1SwathMapSpectrumAccess, true));
    }
    auto ms2SwathMapSpectrumAccess = mSwathRun->loadSwathMapSpectrumAccessPtr(window.name);
    osSwathMaps.emplace_back(ToffeeSpectrumAccess::toOpenSwathMap(ms2SwathMapSpectrumAccess, false));

    // ---
    // calculate scores
    auto ms2SwathMap = mSwathRun->loadSwathMapPtr(window.name);
    TransitionGroupScorer scorer(
        mChromExtractParams,
        mFeatureFinderParams,
        inWindowTargetedExperiment,
        mWorldToIRTTransformation,
        mIrtToWorldTransformation,
        osSwathMaps,
        mMS1SwathMap,
        ms2SwathMap
    );
    ToffeeScorerImpl functor(scorer);
    ToffeeScorerImpl::InputVecType peptideTransitions;
    peptideTransitions.reserve(transitionMap.size());
    for (const auto& pair : transitionMap) {
        peptideTransitions.emplace_back(std::make_pair(*peptideMap.at(pair.first), pair.second));
    }
    ToffeeScorerImpl::RangeType range(peptideTransitions.begin(), peptideTransitions.end());
    tbb::parallel_reduce(range, functor);
    return functor.sortedScores();
}
}

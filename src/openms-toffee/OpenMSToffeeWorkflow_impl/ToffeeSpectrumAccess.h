/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <string>
#include <vector>

#include <OpenMS/OPENSWATHALGO/DATAACCESS/ISpectrumAccess.h>
#include <OpenMS/OPENSWATHALGO/DATAACCESS/SwathMap.h>

#include <toffee/SwathRun.h>
#include <toffee/SwathMapSpectrumAccess.h>

namespace openmstoffee {

class ToffeeSpectrumAccess :
    public OpenSwath::ISpectrumAccess {
public:
    ToffeeSpectrumAccess(
            const std::shared_ptr<const toffee::SwathMapSpectrumAccess>& swathMap
    );

    /// convert a toffee::SwathMapSpectrumAccess into an OpenSwath::SwathMap
    static OpenSwath::SwathMap
    toOpenSwathMap(
        const std::shared_ptr<const toffee::SwathMapSpectrumAccess> &tofSwathMap,
        bool isMS1
    );
    
    boost::shared_ptr<OpenSwath::ISpectrumAccess>
    lightClone() const final;

    /// Return a pointer to a spectrum at the given id
    OpenSwath::SpectrumPtr
    getSpectrumById(
            int id
    ) final;
    
    /// Return a vector of ids of spectra that are within RT +/- deltaRT
    std::vector<std::size_t>
    getSpectraByRT(
            double RT,
            double deltaRT
    ) const final;
    
    /// Returns the number of spectra available
    size_t
    getNrSpectra() const final;
    
    /// Returns the meta information for a spectrum
    OpenSwath::SpectrumMeta
    getSpectrumMetaById(
            int id
    ) const final;

    /// Return a pointer to a chromatogram at the given id
    OpenSwath::ChromatogramPtr
    getChromatogramById(
            int id
    ) final;
    
    /// Returns the number of chromatograms available
    std::size_t
    getNrChromatograms() const final;
    
    /// Returns the native id of the chromatogram at the given id
    std::string
    getChromatogramNativeID(
            int id
    ) const final;

    /// return a pointer to the underlying data
    inline std::shared_ptr<const toffee::SwathMapSpectrumAccess> swathMap() const { return mSwathMap; }

private:
    std::shared_ptr<const toffee::SwathMapSpectrumAccess> mSwathMap;
};

}

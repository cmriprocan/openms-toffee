/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <string>
#include <vector>

#include <OpenMS/OPENSWATHALGO/DATAACCESS/TransitionExperiment.h>
#include <OpenMS/ANALYSIS/TARGETED/TargetedExperiment.h>

namespace openmstoffee {

class TargetedExperimentOps {
public:
    using PeptideIDMap = std::map<std::string, const OpenSwath::LightCompound*>;
    using PeptideIDTransitionMap = std::map<OpenMS::String, std::vector<OpenSwath::LightTransition> >;

    static OpenSwath::LightTargetedExperiment
    loadLightPQP(
        const std::string &pqpFilePath
    );

    static OpenSwath::LightTargetedExperiment
    loadLightTSV(
        const std::string &tsvFilePath
    );

    static OpenMS::TargetedExperiment
    loadTSV(
        const std::string &tsvFilePath
    );

    static
    OpenSwath::LightTargetedExperiment
    filterToMassRange(
        const OpenSwath::LightTargetedExperiment &targetedExperiment,
        double lowerMZ,
        double upperMZ,
        double minUpperEdgeDist
    );

    static
    PeptideIDMap
    mapPeptidesToIds(
        const OpenSwath::LightTargetedExperiment &targetedExperiment
    );

    static
    PeptideIDTransitionMap
    mapTransitionsToPeptideIds(
        const OpenSwath::LightTargetedExperiment &targetedExperiment
    );
};

}

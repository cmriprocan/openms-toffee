/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <string>
#include <vector>

#include <openms-toffee/OpenMSToffeeWorkflow_impl/ChromatogramExtractor.h>
#include <openms-toffee/OpenMSToffeeWorkflow_impl/TargetedExperimentOps.h>

#include <OpenMS/KERNEL/MSChromatogram.h>
#include <OpenMS/ANALYSIS/MAPMATCHING/TransformationDescription.h>
#include <OpenMS/OPENSWATHALGO/DATAACCESS/TransitionExperiment.h>
#include <OpenMS/ANALYSIS/OPENSWATH/OpenSwathWorkflow.h>

namespace openmstoffee {

class TransitionGroupScorer {
public:
    struct TransitionGroupScores {
        OpenSwath::LightCompound peptide;
        std::vector<OpenSwath::LightTransition> transitions;
        OpenMS::FeatureMap features;
        OpenMS::String id;
    };

    TransitionGroupScorer(
        const OpenMS::ChromExtractParams& chromExtractParams,
        const OpenMS::Param& featureFinderParams,
        const OpenSwath::LightTargetedExperiment &inWindowTargetedExperiment,
        const OpenMS::TransformationDescription& worldToIRTTransformation,
        const OpenMS::TransformationDescription& irtToWorldTransformation,
        const std::vector<OpenSwath::SwathMap>& osSwathMaps,
        const std::shared_ptr<const toffee::SwathMap>& ms1SwathMap,
        const std::shared_ptr<const toffee::SwathMap>& ms2SwathMap
    );

    TransitionGroupScores
    calculate(
        const OpenSwath::LightCompound& peptide,
        const std::vector<OpenSwath::LightTransition> &transitions
    ) const;

private:
    OpenMS::Param mFeatureFinderParams;
    OpenSwath::LightTargetedExperiment mInWindowTargetedExperiment;
    OpenMS::TransformationDescription mWorldToIRTTransformation;
    OpenMS::TransformationDescription mIrtToWorldTransformation;
    std::vector<OpenSwath::SwathMap> mOSSwathMaps;
    std::shared_ptr<const toffee::SwathMap> mMS1SwathMap;
    std::shared_ptr<const toffee::SwathMap> mMS2SwathMap;
    ChromatogramExtractor mChromatogramExtractor;
    OpenMS::MRMTransitionGroupPicker mTransitionGroupPicker;

    using MRMTransitionGroup = OpenMS::MRMTransitionGroup<OpenMS::MSChromatogram, OpenSwath::LightTransition>;
    MRMTransitionGroup
    toTransitionGroup(
        const OpenSwath::LightCompound& peptide,
        const std::vector<OpenSwath::LightTransition> &transitions
    ) const;
};

}

/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include "ChromatogramExtractor.h"

#include <tbb/parallel_reduce.h>
#include <tbb/blocked_range.h>

#include <openms-toffee/io_openms.h>

namespace openmstoffee {
ChromatogramExtractor::ChromatogramExtractor(
    const OpenMS::ChromExtractParams& chromExtractParams
) :
    mChromExtractParams(chromExtractParams) {}

OpenMS::ChromExtractParams
ChromatogramExtractor::defaultParams(
    bool forRTNormalisation
) {
#if 0
    // OpenSwath defaults -- these are currently kept here to show how we depart
    // for the original author's intent. Once we have demonstrate our values are
    // more performant, then we can remove this conditional.
    // TODO: Remove this block of code
    OpenMS::ChromExtractParams cp;
    cp.min_upper_edge_dist = 1.0;
    cp.mz_extraction_window = 0.05;
    cp.ppm = false;
    cp.rt_extraction_window = forRTNormalisation ? -1 : 600.0;
    cp.extraction_function = "tophat";
    cp.extra_rt_extract = 0.0;
#endif
    OpenMS::ChromExtractParams cp;
    cp.min_upper_edge_dist = 1.0;
    cp.mz_extraction_window = 50;
    cp.ppm = true;
    cp.rt_extraction_window = forRTNormalisation ? -1 : 400.0;
    cp.extraction_function = "tophat";
    cp.extra_rt_extract = 0.0;
    return cp;
}

OpenMS::MSChromatogram
ChromatogramExtractor::extractChromatogram(
    const toffee::SwathMap& swathMap,
    const OpenSwath::LightCompound& parentPeptide,
    const OpenSwath::LightTransition& transition,
    const OpenMS::TransformationDescription& irtToWorldTransformation
) const {
    throw_assert(
        !swathMap.isMS1(),
        "This function should only be called with MS2 data"
    );
    throw_assert(
        transition.getPeptideRef() == parentPeptide.id,
        "Invalid peptide"
    );

    // ---
    OpenMS::MSChromatogram chromatogram;
    chromatogram.setChromatogramType(OpenMS::ChromatogramSettings::SELECTED_REACTION_MONITORING_CHROMATOGRAM);
    chromatogram.setNativeID(transition.getNativeID());
    chromatogram.setMetaValue("product_mz", transition.getProductMZ());

    // ---
    // Create precursor and set
    // 1) the target m/z
    // 2) the isolation window (upper/lower)
    // 3) the peptide sequence
    OpenMS::Precursor precursor;
    precursor.setMetaValue("peptide_sequence", parentPeptide.sequence);
    precursor.setMZ(transition.getPrecursorMZ());
    precursor.setIsolationWindowLowerOffset(swathMap.precursorCenterMz() - swathMap.precursorLowerMz());
    precursor.setIsolationWindowUpperOffset(swathMap.precursorUpperMz() - swathMap.precursorCenterMz());
    chromatogram.setPrecursor(precursor);

    // ---
    // Create product and set its m/z
    OpenMS::Product product;
    product.setMZ(transition.getProductMZ());
    chromatogram.setProduct(product);

    // ---
    // extract the raw data from the swath map
    addDataToChromatogram(
        swathMap,
        irtToWorldTransformation,
        parentPeptide.rt,
        transition.getProductMZ(),
        chromatogram
    );

    return chromatogram;
}

OpenMS::MSChromatogram
ChromatogramExtractor::extractMS1Chromatogram(
    const toffee::SwathMap& swathMap,
    const OpenSwath::LightCompound& peptide,
    double massOverCharge,
    const OpenMS::TransformationDescription& irtToWorldTransformation
) const {
    throw_assert(
        swathMap.isMS1(),
        "This function should only be called with MS1 data"
    );
    // ---
    OpenMS::MSChromatogram chromatogram;
    chromatogram.setNativeID(peptide.id + "_Precursor_i0");
    chromatogram.setMetaValue("precursor_mz", massOverCharge);

    // ---
    // extract the raw data from the swath map
    addDataToChromatogram(
        swathMap,
        irtToWorldTransformation,
        peptide.rt,
        massOverCharge,
        chromatogram
    );

    return chromatogram;
}

void
ChromatogramExtractor::addDataToChromatogram(
    const toffee::SwathMap& swathMap,
    const OpenMS::TransformationDescription& irtToWorldTransformation,
    double retentionTimeInIRTSpace,
    double massOverCharge,
    OpenMS::MSChromatogram &chromatogram
) const {
    throw_assert(
        mChromExtractParams.extraction_function == "tophat",
        "Invalid extraction function: " << mChromExtractParams.extraction_function
    );

    // m/z range
    std::shared_ptr<toffee::IMassOverChargeRange> mzRangePtr = nullptr;
    if (mChromExtractParams.ppm) {
        mzRangePtr = std::make_shared<toffee::MassOverChargeRangeWithPPMFullWidth>(
            massOverCharge,
            mChromExtractParams.mz_extraction_window
        );
    }
    else {
        double deltaMz = 0.5 * mChromExtractParams.mz_extraction_window;
        mzRangePtr = std::make_shared<toffee::MassOverChargeRange>(
            massOverCharge - deltaMz,
            massOverCharge + deltaMz
        );
    }

    // RT range
    double deNormalizedExperimentalRT = irtToWorldTransformation.apply(retentionTimeInIRTSpace);
    // double deNormalizedExperimentalRT = parentPeptide.rt;
    double lowerRT = deNormalizedExperimentalRT - 0.5 * mChromExtractParams.rt_extraction_window;
    double upperRT = deNormalizedExperimentalRT + 0.5 * mChromExtractParams.rt_extraction_window;
    toffee::RetentionTimeRange rtRange(lowerRT, upperRT);

    auto result = mChromExtractParams.rt_extraction_window > 0
                  ? swathMap.filteredExtractedIonChromatogram(*mzRangePtr, rtRange)
                  : swathMap.extractedIonChromatogram(*mzRangePtr);
    auto& retentionTimeVector = result.retentionTime;
    auto& mzVector = result.massOverCharge;
    auto& intensities = result.intensities;

    for (size_t i = 0; i < retentionTimeVector.size(); ++i) {
        // top hat integration
        double intensity = 0;
        for (size_t j = 0; j < mzVector.size(); ++j) {
            intensity += intensities(i, j);
        }

        OpenMS::ChromatogramPeak peak;
        peak.setRT(retentionTimeVector[i]);
        peak.setIntensity(intensity);
        chromatogram.push_back(peak);
    }
}

}

/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include "TransitionGroupScorer.h"

#include <tbb/parallel_reduce.h>
#include <tbb/blocked_range.h>

#include <openms-toffee/io_openms.h>

namespace openmstoffee {
TransitionGroupScorer::TransitionGroupScorer(
    const OpenMS::ChromExtractParams& chromExtractParams,
    const OpenMS::Param& featureFinderParams,
    const OpenSwath::LightTargetedExperiment &inWindowTargetedExperiment,
    const OpenMS::TransformationDescription& worldToIRTTransformation,
    const OpenMS::TransformationDescription& irtToWorldTransformation,
    const std::vector<OpenSwath::SwathMap>& osSwathMaps,
    const std::shared_ptr<const toffee::SwathMap>& ms1SwathMap,
    const std::shared_ptr<const toffee::SwathMap>& ms2SwathMap
) :
    mFeatureFinderParams(featureFinderParams),
    mInWindowTargetedExperiment(inWindowTargetedExperiment),
    mWorldToIRTTransformation(worldToIRTTransformation),
    mIrtToWorldTransformation(irtToWorldTransformation),
    mOSSwathMaps(osSwathMaps),
    mMS1SwathMap(ms1SwathMap),
    mMS2SwathMap(ms2SwathMap),
    mChromatogramExtractor(chromExtractParams),
    mTransitionGroupPicker()
{
    mTransitionGroupPicker.setParameters(mFeatureFinderParams.copy("TransitionGroupPicker:", true));
}

TransitionGroupScorer::TransitionGroupScores
TransitionGroupScorer::calculate(
    const OpenSwath::LightCompound& peptide,
    const std::vector<OpenSwath::LightTransition> &transitions
) const {
    // set up the transition groups
    auto transitionGroup = toTransitionGroup(peptide, transitions);

    // create copies to avoid const issues with OpenMS
    auto worldToIRTTransformation = mWorldToIRTTransformation;

    TransitionGroupScores scores;
    scores.peptide = peptide;
    scores.id = peptide.id;

    // this is done here to prevent thread-safety issues in OpenSwath
    OpenMS::MRMFeatureFinderScoring featureFinder;
    featureFinder.setParameters(mFeatureFinderParams);
    featureFinder.prepareProteinPeptideMaps_(mInWindowTargetedExperiment);
    if (mMS1SwathMap) {
        auto ms1OpenMSMap = mOSSwathMaps.front();
        throw_assert(
            ms1OpenMSMap.ms1,
            "You must have correctly set up the swath map vector"
        );
        featureFinder.setMS1Map(ms1OpenMSMap.sptr);
    }
    featureFinder.scorePeakgroups(
        transitionGroup,
        worldToIRTTransformation,
        mOSSwathMaps,
        scores.features,
        /*ms1only=*/false
    );
    if (!scores.features.empty()) {
        scores.transitions = transitions;
    }

    return scores;
}

TransitionGroupScorer::MRMTransitionGroup
TransitionGroupScorer::toTransitionGroup(
    const OpenSwath::LightCompound& peptide,
    const std::vector<OpenSwath::LightTransition> &transitions
) const {
    MRMTransitionGroup transitionGroup;
    transitionGroup.setTransitionGroupID(peptide.id);

    bool addedMS1 = false;
    for (const auto& transition : transitions) {
        if (mMS1SwathMap && !addedMS1) {
            addedMS1 = true;
            auto ms1Chromatogram = mChromatogramExtractor.extractMS1Chromatogram(
                *mMS1SwathMap,
                peptide,
                transition.getPrecursorMZ(),
                mIrtToWorldTransformation
            );
            transitionGroup.addPrecursorChromatogram(
                ms1Chromatogram,
                ms1Chromatogram.getNativeID()
            );
        }

        // MS2 data
        auto chromatogram = mChromatogramExtractor.extractChromatogram(
            *mMS2SwathMap,
            peptide,
            transition,
            mIrtToWorldTransformation
        );
        transitionGroup.addTransition(transition, transition.getNativeID());
        transitionGroup.addChromatogram(chromatogram, chromatogram.getNativeID());
    }
    throw_assert(transitionGroup.isInternallyConsistent(), "Invalid transition group");

    // create a copy to prevent thread-safety issues in OpenSwath
    auto transitionGroupPicker = mTransitionGroupPicker;
    transitionGroupPicker.pickTransitionGroup(transitionGroup);

    return transitionGroup;
}
}

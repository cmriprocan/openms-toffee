/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include "TargetedExperimentOps.h"

#include <OpenMS/ANALYSIS/OPENSWATH/TransitionPQPFile.h>
#include <OpenMS/ANALYSIS/OPENSWATH/TransitionTSVFile.h>
#include <OpenMS/FORMAT/TraMLFile.h>
#include <OpenMS/ANALYSIS/OPENSWATH/OpenSwathWorkflow.h>

#include <openms-toffee/io_openms.h>

namespace openmstoffee {

OpenSwath::LightTargetedExperiment
TargetedExperimentOps::loadLightPQP(
    const std::string &pqpFilePath
) {
    OpenSwath::LightTargetedExperiment transitionExp;
    OpenMS::TransitionPQPFile().convertPQPToTargetedExperiment(pqpFilePath.c_str(), transitionExp);
    return transitionExp;
}

OpenSwath::LightTargetedExperiment
TargetedExperimentOps::loadLightTSV(
    const std::string &tsvFilePath
) {
    OpenSwath::LightTargetedExperiment transitionExp;
    OpenMS::TransitionTSVFile().convertTSVToTargetedExperiment(
            tsvFilePath.c_str(),
            OpenMS::FileTypes::Type::TSV,
            transitionExp
    );
    return transitionExp;
}

OpenMS::TargetedExperiment
TargetedExperimentOps::loadTSV(
    const std::string &tsvFilePath
)
{
    OpenMS::TargetedExperiment transitionExp;
    OpenMS::TransitionTSVFile().convertTSVToTargetedExperiment(
            tsvFilePath.c_str(),
            OpenMS::FileTypes::Type::TSV,
            transitionExp
    );
    return transitionExp;
}

OpenSwath::LightTargetedExperiment
TargetedExperimentOps::filterToMassRange(
    const OpenSwath::LightTargetedExperiment &targetedExperiment,
    double lowerMZ,
    double upperMZ,
    double minUpperEdgeDist
) {
    OpenSwath::LightTargetedExperiment inWindowExperiment;
    OpenMS::OpenSwathHelper::selectSwathTransitions(
        targetedExperiment,
        inWindowExperiment,
        minUpperEdgeDist,
        lowerMZ,
        upperMZ
    );
    return inWindowExperiment;
}

TargetedExperimentOps::PeptideIDMap
TargetedExperimentOps::mapPeptidesToIds(
    const OpenSwath::LightTargetedExperiment &targetedExperiment
) {
    PeptideIDMap result;
    for (auto& peptide : targetedExperiment.getCompounds()) {
        result[peptide.id] = &peptide;
    }
    return result;
}

TargetedExperimentOps::PeptideIDTransitionMap
TargetedExperimentOps::mapTransitionsToPeptideIds(
    const OpenSwath::LightTargetedExperiment &targetedExperiment
) {
    PeptideIDTransitionMap result;
    for (const auto& transition : targetedExperiment.getTransitions()) {
        result[transition.getPeptideRef()].emplace_back(transition);
    }
    return result;
}
}

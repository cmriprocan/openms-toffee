/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include "ToffeeSpectrumAccess.h"
#include <numeric>
#include <boost/make_shared.hpp>
#include <openms-toffee/io_openms.h>

namespace openmstoffee {
ToffeeSpectrumAccess::ToffeeSpectrumAccess(
        const std::shared_ptr<const toffee::SwathMapSpectrumAccess>& swathMap
) :
        mSwathMap(swathMap) {
}

OpenSwath::SwathMap
ToffeeSpectrumAccess::toOpenSwathMap(
    const std::shared_ptr<const toffee::SwathMapSpectrumAccess> &tofSwathMap,
    bool isMS1
) {
    OpenSwath::SwathMap osSwathMap;
    osSwathMap.lower = tofSwathMap->precursorLowerMz();
    osSwathMap.center = tofSwathMap->precursorCenterMz();
    osSwathMap.upper = tofSwathMap->precursorUpperMz();
    osSwathMap.ms1 = isMS1;
    osSwathMap.sptr = boost::make_shared<ToffeeSpectrumAccess>(tofSwathMap);
    return osSwathMap;
}

boost::shared_ptr<OpenSwath::ISpectrumAccess>
ToffeeSpectrumAccess::lightClone() const {
    return boost::make_shared<ToffeeSpectrumAccess>(mSwathMap);
}

/// Return a pointer to a spectrum at the given id
OpenSwath::SpectrumPtr
ToffeeSpectrumAccess::getSpectrumById(
        int id
) {
    auto s = mSwathMap->spectrumByIndex(static_cast<size_t>(id));

    auto mz = boost::make_shared<OpenSwath::BinaryDataArray>();
    mz->data = std::vector<double>(
        s.massOverCharge.data(),
        s.massOverCharge.data() + s.massOverCharge.size()
    );
    auto intensity = boost::make_shared<OpenSwath::BinaryDataArray>();
    intensity->data = std::vector<double>(
        s.intensities.data(),
        s.intensities.data() + s.intensities.size()
    );

    auto spectrum = boost::make_shared<OpenSwath::Spectrum>();
    spectrum->setMZArray(mz);
    spectrum->setIntensityArray(intensity);
    return spectrum;
}

/// Return a vector of ids of spectra that are within RT +/- deltaRT
std::vector<std::size_t>
ToffeeSpectrumAccess::getSpectraByRT(
        double RT,
        double deltaRT
) const {
    const auto &rt = mSwathMap->retentionTime();
    auto lower = toffee::IRetentionTimeRange::toRTIdx(
        RT - deltaRT,
        rt
    );
    auto upper = toffee::IRetentionTimeRange::toRTIdx(
        RT + deltaRT,
        rt
    );
    std::vector<std::size_t> result(upper - lower + 1);
    std::iota(result.begin(), result.end(), lower);
    assert(result.front() == lower);
    assert(result.back() == upper);
    return result;
}

/// Returns the number of spectra available
size_t
ToffeeSpectrumAccess::getNrSpectra() const {
    return mSwathMap->numberOfSpectra();
}

/// Returns the meta information for a spectrum
OpenSwath::SpectrumMeta
ToffeeSpectrumAccess::getSpectrumMetaById(
        int id
) const {
    assert(id >= 0 && static_cast<size_t>(id) < mSwathMap->numberOfSpectra());
    OpenSwath::SpectrumMeta result;
    result.RT = mSwathMap->retentionTime()[id];
    result.ms_level = mSwathMap->isMS1();
    return result;
}

/// Return a pointer to a chromatogram at the given id
OpenSwath::ChromatogramPtr
ToffeeSpectrumAccess::getChromatogramById(
        int id
) {
    throw_assert(false, "Not implemented");
}

/// Returns the number of chromatograms available
std::size_t
ToffeeSpectrumAccess::getNrChromatograms() const {
    throw_assert(false, "Not implemented");
}

/// Returns the native id of the chromatogram at the given id
std::string
ToffeeSpectrumAccess::getChromatogramNativeID(
        int id
) const {
    throw_assert(false, "Not implemented");
}

}

/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <string>
#include <vector>

#include <toffee/SwathRun.h>
#include <toffee/SwathMap.h>
#include <toffee/SwathMapSpectrumAccess.h>
#include <openms-toffee/OpenMSToffeeWorkflow_impl/TransitionGroupScorer.h>

#include <OpenMS/ANALYSIS/MAPMATCHING/TransformationDescription.h>
#include <OpenMS/OPENSWATHALGO/DATAACCESS/TransitionExperiment.h>
#include <OpenMS/OPENSWATHALGO/DATAACCESS/DataStructures.h>
#include <OpenMS/ANALYSIS/OPENSWATH/MRMFeatureFinderScoring.h>
#include <OpenMS/ANALYSIS/OPENSWATH/OpenSwathOSWWriter.h>
#include <OpenMS/ANALYSIS/OPENSWATH/OpenSwathWorkflow.h>

namespace openmstoffee {

class ToffeeScorer {
public:
    ToffeeScorer();

    void
    setToffeeFilePath(
        const std::string& toffeeFilePath,
        bool loadMS1
    );

    std::shared_ptr<const toffee::SwathRun> swathRun() const { return mSwathRun; }

    bool useMS1() const { return mMS1SwathMap != nullptr; }

    void
    updateChromExtractParams(
        const OpenMS::ChromExtractParams &param
    );

    void
    updateFeatureFinderParams(
        const OpenMS::Param &param
    );

    void
    setWorldToIRTTransformation(
        const OpenMS::TransformationDescription& worldToIRTTransformation
    );

    void
    setTargetedExperiment(
        const OpenSwath::LightTargetedExperiment& targetedExperiment
    );

    std::vector<TransitionGroupScorer::TransitionGroupScores>
    calculateScores(
        const toffee::MS2WindowDescriptor& window
    ) const;

    // Each of these returns an object full of default parameters for
    // the indicated phase of the analysis.
    static OpenMS::Param defaultFeatureFinderParams(
        bool forRTNormalisation
    );

private:
    std::string mToffeeFilePath;
    std::shared_ptr<const toffee::SwathRun> mSwathRun;
    std::shared_ptr<toffee::SwathMap> mMS1SwathMap;
    std::shared_ptr<toffee::SwathMapSpectrumAccess> mMS1SwathMapSpectrumAccess;
    OpenMS::ChromExtractParams mChromExtractParams;
    OpenMS::Param mFeatureFinderParams;
    OpenMS::TransformationDescription mWorldToIRTTransformation;
    OpenMS::TransformationDescription mIrtToWorldTransformation;
    OpenSwath::LightTargetedExperiment mTargetedExperiment;
};

}

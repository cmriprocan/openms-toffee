/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <string>
#include <vector>

#include <toffee/SwathMap.h>

#include <OpenMS/KERNEL/MSChromatogram.h>
#include <OpenMS/ANALYSIS/MAPMATCHING/TransformationDescription.h>
#include <OpenMS/OPENSWATHALGO/DATAACCESS/TransitionExperiment.h>
#include <OpenMS/ANALYSIS/OPENSWATH/OpenSwathWorkflow.h>

namespace openmstoffee {

class ChromatogramExtractor {
public:

    explicit ChromatogramExtractor(
        const OpenMS::ChromExtractParams& chromExtractParams
    );

    static OpenMS::ChromExtractParams
    defaultParams(
        bool forRTNormalisation
        );

    OpenMS::MSChromatogram
    extractChromatogram(
        const toffee::SwathMap& swathMap,
        const OpenSwath::LightCompound& parentPeptide,
        const OpenSwath::LightTransition& transition,
        const OpenMS::TransformationDescription& irtToWorldTransformation
    ) const;

    OpenMS::MSChromatogram
    extractMS1Chromatogram(
        const toffee::SwathMap& swathMap,
        const OpenSwath::LightCompound& peptide,
        double massOverCharge,
        const OpenMS::TransformationDescription& irtToWorldTransformation
    ) const;

private:
    OpenMS::ChromExtractParams mChromExtractParams;

    void
    addDataToChromatogram(
        const toffee::SwathMap& swathMap,
        const OpenMS::TransformationDescription& irtToWorldTransformation,
        double retentionTimeInIRTSpace,
        double massOverCharge,
        OpenMS::MSChromatogram &chromatogram
    ) const;
};

}

# Change Log
  
## 0.15

### 0.15.3

- PD-1292 SRL Conversion (IRT Creation) includes supplied peptide sequences
   * Added --extra-irt-peptide /peptide/ argument to specify one peptide
   ** Can be aliased by -e for brevity
   ** Can be specified multiple times so you're not limited to 1
   * Added --extra-irt-file /file/ to allow a whole file full of extra peptides if you prefer

### 0.15.2

- PD-1112 add support for new irt peptides
   * Changed from using a fixed set of variables for the different masks to building up a mask from a list
   * Added ProCal to the list of mask strings

### 0.15.1

- Branched from 0.14.2
- Reapplied applicable changes b71873e:
   - Bumped base image to `1.2.5` to include gnuparallel
   - Removed bitbucket user as repo now public

## 0.14

### 0.14.2

- Improved robustness for converting Sciex SRLs (PD-896)
- Bumped version of `toffee` to 0.14.2

### 0.14.1

- Changed license to MIT and fixed documentation for https://openms-toffee.readthedocs.io
- Bumped version of `toffee` to 0.13.1, also MIT licensed

## 0.13

### 0.13.12

- Bumped version of `toffee` to 0.12.16, which now includes the mzML to toffee to mzML conversions
- Removed all mzML to toffee conversions from this package and deleted tests

### 0.13.11

- Bumped version of `toffee` to 0.12.9, which now includes the toffee to mzML conversion (PD-793)

### 0.13.10

- Bumped version of `toffee` to 0.12.4
- Many fixes to accommodate change in structure of the dia-test-data repository

### 0.13.9

- Added ability to convert toffee files to Spectra HDF5 files (PD-793)

### 0.13.8

- Bumped version of `toffee` to 0.11.1

### 0.13.6

- Bumped version of `toffee` to 0.10.6 (that fixes PD-749) and added unit test

### 0.13.5

- Bumped version of `toffee` to 0.10.5 (that fixes PD-735)

### 0.13.4

- Pinned version of `toffee` to 0.10.4 (that fixes PD-727)

### 0.13.3

- A few bug fixes for the mzML to toffee conversion

### 0.13.2

- Updated to version 1.2.4 of `cmriprocan/openms`.

### 0.13.1

- Significant refactor of the `XML` to `toffee` conversion process. This has been pulled apart into smaller modules such that individual parts can be tested. Importantly, the code that converts `mzML` and `mzXML` to `HDF5` is now shared by the process that then converts these to toffee. This improves memory performance, and means that we can test both conversions.

## 0.12

### 0.12.1

- Directly linking to upstream image in Dockerfile: `cmriprocan/openms:1.2.3` which:
  - Updated `OpenMS` to version `CMRI-ProCan-v1.1.2`
  - Updated `PyProphet` to `2.0.2`

## 0.11

### 0.11.1

- Updated `OpenMS` to version `CMRI-ProCan-v1.1.1` with hot-fix to solve https://github.com/OpenMS/OpenMS/issues/3860

## 0.10

### 0.10.1

- Updated `OpenMS` to version `CMRI-ProCan-v1.1.0` that incorporates upstream `Release2.4.0` with our changes that enable toffee files to be used on the command line

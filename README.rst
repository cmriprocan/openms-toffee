OpenMSToffee
============

For a full description of ``openms-toffee`` please see https://openms-toffee.readthedocs.io, or within the development environment, run the following

.. code:: bash
    :linenos:

        python setup.py build_sphinx

.. inclusion-marker-do-not-remove

|Toffee Logo|


|License| |ReadTheDocs| || Master: |Master CircleCI Status| || Dev: |Dev CircleCI Status| 

``OpenMSToffee`` is a wrapper around ``OpenSwath`` that allows us to use the toffee_ file format. It is available as a Docker image at ``cmriprocan/openms-toffee``.

We follow the `OpenVDB style guide <http://www.openvdb.org/documentation/doxygen/codingStyle.html>`_ for the C++ and PEP-8 for our python code, so please aim to stay consistent with the rest of the code base. Contributions will be pass through peer review and style will be one element that is reviewed.

Changes
-------

.. mdinclude:: CHANGES.md

License
-------

`MIT`_ Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

.. _MIT: ../LICENSE
.. _toffee: https://toffee.readthedocs.io


.. |Master CircleCI Status| image:: https://circleci.com/bb/cmriprocan/openms-toffee/tree/master.svg?style=svg&circle-token=02f364907c2ca849d5907126611d3d6bbe538a6b
    :target: https://circleci.com/bb/cmriprocan/openms-toffee/tree/master
    :scale: 100%
    :alt: Master Build Status

.. |Dev CircleCI Status| image:: https://circleci.com/bb/cmriprocan/openms-toffee/tree/dev.svg?style=svg&circle-token=02f364907c2ca849d5907126611d3d6bbe538a6b
    :target: https://circleci.com/bb/cmriprocan/openms-toffee/tree/dev
    :scale: 100%
    :alt: Dev Build Status

.. |License| image:: https://img.shields.io/badge/License-MIT-blue.svg
    :target: https://opensource.org/licenses/MIT
    :scale: 100%
    :alt: MIT license

.. |ReadTheDocs| image:: https://readthedocs.org/projects/openms-toffee/badge/?version=latest
    :target: https://openms-toffee.readthedocs.io/en/latest/?badge=latest
    :scale: 100%
    :alt: Documentation Status

.. |Toffee Logo| image:: ../Toffee.svg
    :target: https://openms-toffee.readthedocs.io
    :scale: 20%
    :alt: Toffee

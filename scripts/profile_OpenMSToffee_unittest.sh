#!/bin/bash
set -x -e

if [ $# != 0 ]
then
    TEST_FILTER="$@"
else
    TEST_FILTER=*
fi

EXE=../pysrc/OpenMSToffee/OpenMSToffee_unittest

LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libprofiler.so CPUPROFILE=cpu_profile.prof ${EXE} --gtest_filter=${TEST_FILTER} \
 && google-pprof --pdf ${EXE} cpu_profile.prof > cpu_profile.pdf


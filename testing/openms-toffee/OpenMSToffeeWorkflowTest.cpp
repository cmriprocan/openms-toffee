/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include "OpenMSToffeeWorkflowTest.h"
#include <openms-toffee/io_openms.h>

namespace openmstoffee {
void
OpenMSToffeeWorkflowTest::runConfigTest(
    std::vector<std::string> vargs,
    OpenMS::TOPPBase::ExitCodes expectedCode
) {
    // convert to argv
    std::vector<const char*> argv;
    argv.reserve(vargs.size());
    std::transform(
        vargs.cbegin(),
        vargs.cend(),
        std::back_inserter(argv),
        [](const auto& s) { return s.c_str(); }
    );
    auto argc = static_cast<int>(argv.size());

    OpenMSToffeeWorkflow wf(/*testing=*/true);
    auto exitCode = wf.main(argc, argv.data());
    EXPECT_EQ(expectedCode, exitCode);
}
}

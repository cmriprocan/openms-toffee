/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include <gtest/gtest.h>
#include <testing/openms-toffee/OpenMSToffeeWorkflowTest.h>

namespace openmstoffee {
TEST_F(OpenMSToffeeWorkflowTest, testProCan90RunAPQP) {
    ProCan90TestData testData;
    bool useRunA = true;
    bool usePQP = true;
    bool useFilteredSRL = false;
    runOpenMSToffeeWorkflow(useRunA, usePQP, useFilteredSRL, testData);
}

TEST_F(OpenMSToffeeWorkflowTest, testProCan90RunBPQP) {
    ProCan90TestData testData;
    bool useRunA = false;
    bool usePQP = true;
    bool useFilteredSRL = false;
    runOpenMSToffeeWorkflow(useRunA, usePQP, useFilteredSRL, testData);
}

TEST_F(OpenMSToffeeWorkflowTest, testProCan90RunATSV) {
    ProCan90TestData testData;
    bool useRunA = true;
    bool usePQP = false;
    bool useFilteredSRL = false;
    runOpenMSToffeeWorkflow(useRunA, usePQP, useFilteredSRL, testData);
}

TEST_F(OpenMSToffeeWorkflowTest, testProCan90RunBTSV) {
    ProCan90TestData testData;
    bool useRunA = false;
    bool usePQP = false;
    bool useFilteredSRL = false;
    runOpenMSToffeeWorkflow(useRunA, usePQP, useFilteredSRL, testData);
}

TEST_F(OpenMSToffeeWorkflowTest, testSGS2014RunATSV) {
    SGS2014TestData testData;
    bool useRunA = true;
    bool usePQP = false;
    bool useFilteredSRL = false;
    runOpenMSToffeeWorkflow(useRunA, usePQP, useFilteredSRL, testData);
}

TEST_F(OpenMSToffeeWorkflowTest, testSGS2014RunBTSV) {
    SGS2014TestData testData;
    bool useRunA = false;
    bool usePQP = false;
    bool useFilteredSRL = false;
    runOpenMSToffeeWorkflow(useRunA, usePQP, useFilteredSRL, testData);
}
}

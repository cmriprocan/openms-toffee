/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include <testing/openms-toffee/OpenMSToffeeWorkflowTest.h>
#include <OpenMS/APPLICATIONS/TOPPBase.h>
#include <openms-toffee/OpenMSToffeeWorkflow.h>

namespace openmstoffee {

// ==========================================
// Test configuration
// ==========================================

TEST_F(OpenMSToffeeWorkflowTest, MissingArguments) {
    runConfigTest(
        buildArgs_missing_parameters(),
        OpenMS::TOPPBase::ExitCodes::ILLEGAL_PARAMETERS
    );
}

TEST_F(OpenMSToffeeWorkflowTest, LogArguments) {
    runConfigTest(
        buildArgs_log_arguments(),
        OpenMS::TOPPBase::ExitCodes::MISSING_PARAMETERS
    );
}

TEST_F(OpenMSToffeeWorkflowTest, InFile) {
    runConfigTest(
        buildArgs_file_arguments(bad, good, good, good),
        OpenMS::TOPPBase::ExitCodes::INPUT_FILE_NOT_FOUND
    );
}

TEST_F(OpenMSToffeeWorkflowTest, SrlFile) {
    runConfigTest(
        buildArgs_file_arguments(good, bad, good, good),
        OpenMS::TOPPBase::ExitCodes::INPUT_FILE_NOT_FOUND
    );
}

TEST_F(OpenMSToffeeWorkflowTest, IrtFile) {
    runConfigTest(
        buildArgs_file_arguments(good, good, bad, good),
        OpenMS::TOPPBase::ExitCodes::INPUT_FILE_NOT_FOUND
    );
}

TEST_F(OpenMSToffeeWorkflowTest, OutFile) {
    runConfigTest(
        buildArgs_file_arguments(good, good, good, bad),
        OpenMS::TOPPBase::ExitCodes::CANNOT_WRITE_OUTPUT_FILE
    );
}

TEST_F(OpenMSToffeeWorkflowTest, Works) {
    runConfigTest(
        buildArgs_file_arguments(good, good, good, good),
        OpenMS::TOPPBase::ExitCodes::EXECUTION_OK
    );
}

TEST_F(OpenMSToffeeWorkflowTest, ChromExtractParams) {
    // This only tests the argument handling, not whether the
    // supplied values are used.
    runConfigTest(
        buildArgs_chromExtractParams(),
        OpenMS::TOPPBase::ExitCodes::EXECUTION_OK
    );
}

TEST_F(OpenMSToffeeWorkflowTest, MassUnitFlagFailure) {
    // This only tests the argument handling, not whether the
    // supplied values are used.
    runConfigTest(
        buildArgs_MassUnitFlagFailure(),
        OpenMS::TOPPBase::ExitCodes::ILLEGAL_PARAMETERS
    );
}

TEST_F(OpenMSToffeeWorkflowTest, RTNormBothSpecified) {
    std::vector<std::string> vargs;
    add_program_name(vargs);
    add_log_arguments(vargs);
    add_input_file(vargs, good.at(Input));
    add_srl_file(vargs, good.at(SRL));
    add_irt_file(vargs, good.at(iRT));
    add_output_file(vargs, good.at(Output));
    add_arg(vargs, "-rt_norm", good.at(RTNorm));
    runConfigTest(
        vargs,
        OpenMS::TOPPBase::ExitCodes::ILLEGAL_PARAMETERS
    );
}

TEST_F(OpenMSToffeeWorkflowTest, RTNormNoneSpecified) {
    std::vector<std::string> vargs;
    add_program_name(vargs);
    add_log_arguments(vargs);
    add_input_file(vargs, good.at(Input));
    add_srl_file(vargs, good.at(SRL));
    add_output_file(vargs, good.at(Output));
    runConfigTest(
        vargs,
        OpenMS::TOPPBase::ExitCodes::ILLEGAL_PARAMETERS
    );
}

TEST_F(OpenMSToffeeWorkflowTest, RTNormCorrectlySpecified) {
    std::vector<std::string> vargs;
    add_program_name(vargs);
    add_log_arguments(vargs);
    add_input_file(vargs, good.at(Input));
    add_srl_file(vargs, good.at(SRL));
    add_output_file(vargs, good.at(Output));
    add_arg(vargs, "-rt_norm", good.at(RTNorm));
    runConfigTest(
        vargs,
        OpenMS::TOPPBase::ExitCodes::EXECUTION_OK
    );
}

// ==========================================
// Test running the workflow
// ==========================================

TEST_F(OpenMSToffeeWorkflowTest, testProCan90RunAPQP) {
    ProCan90TestData testData;
    bool useRunA = true;
    bool usePQP = true;
    bool useFilteredSRL = true;
    runOpenMSToffeeWorkflow(useRunA, usePQP, useFilteredSRL, testData);
}

TEST_F(OpenMSToffeeWorkflowTest, testProCan90RunBPQP) {
    ProCan90TestData testData;
    bool useRunA = false;
    bool usePQP = true;
    bool useFilteredSRL = true;
    runOpenMSToffeeWorkflow(useRunA, usePQP, useFilteredSRL, testData);
}

TEST_F(OpenMSToffeeWorkflowTest, testProCan90RunATSV) {
    ProCan90TestData testData;
    bool useRunA = true;
    bool usePQP = false;
    bool useFilteredSRL = true;
    runOpenMSToffeeWorkflow(useRunA, usePQP, useFilteredSRL, testData);
}

TEST_F(OpenMSToffeeWorkflowTest, testProCan90RunBTSV) {
    ProCan90TestData testData;
    bool useRunA = false;
    bool usePQP = false;
    bool useFilteredSRL = true;
    runOpenMSToffeeWorkflow(useRunA, usePQP, useFilteredSRL, testData);
}

TEST_F(OpenMSToffeeWorkflowTest, testSGS2014RunAPQP) {
    SGS2014TestData testData;
    bool useRunA = true;
    bool usePQP = true;
    bool useFilteredSRL = true;
    runOpenMSToffeeWorkflow(useRunA, usePQP, useFilteredSRL, testData);
}

TEST_F(OpenMSToffeeWorkflowTest, testSGS2014RunATSV) {
    SGS2014TestData testData;
    bool useRunA = true;
    bool usePQP = false;
    bool useFilteredSRL = true;
    runOpenMSToffeeWorkflow(useRunA, usePQP, useFilteredSRL, testData);
}
}

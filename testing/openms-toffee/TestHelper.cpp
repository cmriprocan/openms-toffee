/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include "TestHelper.h"
#include <boost/filesystem.hpp>
#include <toffee/io.h>
#include <fstream>
#include <algorithm>

namespace fs = boost::filesystem;

namespace openmstoffee {
std::string
TestHelper::basename(
        const std::string &filepath
)
{
    return fs::path(filepath).filename().string();
}

bool
TestHelper::filesAreEqual(
        const std::string &fileA,
        const std::string &fileB
)
{
    // code borrowed from: https://stackoverflow.com/a/39097160
    std::ifstream file1(fileA, std::ifstream::ate | std::ifstream::binary); //open file at the end
    std::ifstream file2(fileB, std::ifstream::ate | std::ifstream::binary); //open file at the end
    const std::ifstream::pos_type fileSize = file1.tellg();

    if (fileSize != file2.tellg()) {
        return false; //different file size
    }

    file1.seekg(0); //rewind
    file2.seekg(0); //rewind

    std::istreambuf_iterator<char> begin1(file1);
    std::istreambuf_iterator<char> begin2(file2);

    return std::equal(begin1, std::istreambuf_iterator<char>(), begin2); //Second argument is end-of-range iterator
}

ProCan90TestData::ProCan90TestData() :
    name{"ProCan90"},
    basePath{std::string{TEST_DATA_DIR} + std::string{"/ProCan90/"}},
    runATofPath{basePath + std::string{"tof/ProCan90-M03-01.tof"}},
    runBTofPath{basePath + std::string{"tof/ProCan90-M06-07.tof"}},
    runARetentionTimeTofPath{basePath + std::string{"tof/ProCan90-M03-01.iRT.tof"}},
    runBRetentionTimeTofPath{basePath + std::string{"tof/ProCan90-M06-07.iRT.tof"}},
    srlTSVPath{basePath + std::string{"srl/hek_srl.OpenSwath.reverse_decoys.tsv"}},
    srlPQPPath{basePath + std::string{"srl/hek_srl.OpenSwath.reverse_decoys.pqp"}},
    alignmentTSVPath{basePath + std::string{"srl/hek_srl.OpenSwath.iRT.tsv"}},
    alignmentPQPPath{basePath + std::string{"srl/hek_srl.OpenSwath.iRT.pqp"}},
    filteredSrlTSVPath{alignmentTSVPath},
    filteredSrlPQPPath{alignmentPQPPath},
    regressionBasePath{std::string{TESTING_DIR} + std::string{"/openms-toffee/test_data/"}},
    testCalculateStringsForOutputSQLite{regressionBasePath + std::string{"testCalculateStringsForOutputSQLite.pqp"}},
    testCalculateStringsForOutputTSV{regressionBasePath + std::string{"testCalculateStringsForOutputTSV.tsv"}},
    runATrafoXML{basePath + std::string{"srl/ProCan90-M03-01.trafoXML"}},
    runBTrafoXML{basePath + std::string{"srl/ProCan90-M06-07.trafoXML"}}
{}

OpenMS::TransformationDescription
ProCan90TestData::runATransformation()
{
    OpenMS::Param params;
    params.setValue("symmetric_regression", "false");
    params.setValue("x_weight", "");
    params.setValue("y_weight", "");
    params.setValue("x_datum_min", 1e-15);
    params.setValue("x_datum_max", 1e15);
    params.setValue("y_datum_min", 1e-15);
    params.setValue("y_datum_max", 1e15);
    params.setValue("span", 2.0 / 3.0);
    params.setValue("num_nodes", 5);
    params.setValue("slope", 0.015810807077921062);
    params.setValue("intercept", 5.0670993097122601);

    OpenMS::TransformationDescription result;
    result.fitModel("linear", params);
    return result;
}

OpenMS::TransformationDescription
ProCan90TestData::runBTransformation()
{
    OpenMS::Param params;
    params.setValue("symmetric_regression", "false");
    params.setValue("x_weight", "");
    params.setValue("y_weight", "");
    params.setValue("x_datum_min", 1e-15);
    params.setValue("x_datum_max", 1e15);
    params.setValue("y_datum_min", 1e-15);
    params.setValue("y_datum_max", 1e15);
    params.setValue("span", 2.0 / 3.0);
    params.setValue("num_nodes", 5);
    params.setValue("slope", 0.015321837714345338);
    params.setValue("intercept", 2.0588952268325365);

    OpenMS::TransformationDescription result;
    result.fitModel("linear", params);
    return result;
}

SGS2014TestData::SGS2014TestData() :
    name{"SGS2014"},
    basePath{std::string{TEST_DATA_DIR} + std::string{"/SwathGoldStandard/"}},
    runATofPath{basePath + std::string{"tof/napedro_l120224_002_sw.tof"}},
    runBTofPath{basePath + std::string{"tof/napedro_l120224_010_sw.tof"}},
    runARetentionTimeTofPath{basePath + std::string{"tof/napedro_l120224_002_sw.iRT.tof"}},
    runBRetentionTimeTofPath{basePath + std::string{"tof/napedro_l120224_010_sw.iRT.tof"}},
    srlTSVPath{basePath + std::string{"srl/OpenSWATH_SM4_GoldStandardAssayLibrary.tsv"}},
    srlPQPPath{basePath + std::string{"srl/OpenSWATH_SM4_GoldStandardAssayLibrary.pqp"}},
    alignmentTSVPath{basePath + std::string{"srl/OpenSWATH_SM4_iRT_AssayLibrary.tsv"}},
    alignmentPQPPath{basePath + std::string{"srl/OpenSWATH_SM4_iRT_AssayLibrary.pqp"}},
    filteredSrlTSVPath{alignmentTSVPath},
    filteredSrlPQPPath{alignmentPQPPath},
    runATrafoXML{basePath + std::string{"srl/napedro_l120224_002_sw.trafoXML"}},
    runBTrafoXML{basePath + std::string{"srl/napedro_l120224_010_sw.trafoXML"}}
{}

OpenMS::TransformationDescription
SGS2014TestData::runATransformation()
{
    OpenMS::Param params;
    params.setValue("symmetric_regression", "false");
    params.setValue("x_weight", "");
    params.setValue("y_weight", "");
    params.setValue("x_datum_min", 1e-15);
    params.setValue("x_datum_max", 1e15);
    params.setValue("y_datum_min", 1e-15);
    params.setValue("y_datum_max", 1e15);
    params.setValue("span", 2.0 / 3.0);
    params.setValue("num_nodes", 5);
    params.setValue("slope", 0.029462562703369541);
    params.setValue("intercept", -64.720279128052027);

    OpenMS::TransformationDescription result;
    result.fitModel("linear", params);
    return result;
}

OpenMS::TransformationDescription
SGS2014TestData::runBTransformation()
{
    OpenMS::Param params;
    params.setValue("symmetric_regression", "false");
    params.setValue("x_weight", "");
    params.setValue("y_weight", "");
    params.setValue("x_datum_min", 1e-15);
    params.setValue("x_datum_max", 1e15);
    params.setValue("y_datum_min", 1e-15);
    params.setValue("y_datum_max", 1e15);
    params.setValue("span", 2.0 / 3.0);
    params.setValue("num_nodes", 5);
    params.setValue("slope", 0.029543802964597753);
    params.setValue("intercept", -65.340074415437186);

    OpenMS::TransformationDescription result;
    result.fitModel("linear", params);
    return result;
}
}

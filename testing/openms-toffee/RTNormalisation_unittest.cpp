/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include <gtest/gtest.h>
#include <boost/filesystem.hpp>
#include <testing/openms-toffee/TestHelper.h>
#include <openms-toffee/io_openms.h>
#include <openms-toffee/RTNormalisation.h>

namespace openmstoffee {

class RTNormalisationTest :
    public ::testing::Test {
protected:

    template<typename TestData>
    void testLoadRetentionTime(
        bool useRunA,
        TestData testData
    ) {
        auto exactTransformation = useRunA
                                   ? testData.runATransformation()
                                   : testData.runBTransformation();
        const auto& exactParams = exactTransformation.getModelParameters();

        auto tofPath = useRunA
                       ? testData.runARetentionTimeTofPath
                       : testData.runBRetentionTimeTofPath;
        ASSERT_TRUE(boost::filesystem::exists(tofPath)) << tofPath;
        ASSERT_TRUE(boost::filesystem::exists(testData.alignmentTSVPath)) << testData.alignmentTSVPath;
        RTNormalisation rtNormalisation(
            tofPath,
            testData.alignmentTSVPath
        );
        auto irtTransformation = rtNormalisation.calculateNormalisation();
        compareTwoTransformations(exactTransformation, irtTransformation);
    }

    /// If roundTrip is true, the transform is saved to disk and then read
    /// back in. It if is false, the transforms in the test data repo are
    /// used. This will help us keep these in sync for future tests
    template<typename TestData>
    void testCalculateNormalisationFromFile(
        bool useRunA,
        TestData testData,
        bool roundTrip
    ) {
        auto exactTransformation = useRunA
                                   ? testData.runATransformation()
                                   : testData.runBTransformation();

        auto tofPath = useRunA
                       ? testData.runARetentionTimeTofPath
                       : testData.runBRetentionTimeTofPath;
        ASSERT_TRUE(boost::filesystem::exists(tofPath)) << tofPath;
        ASSERT_TRUE(boost::filesystem::exists(testData.alignmentTSVPath)) << testData.alignmentTSVPath;
        RTNormalisation rtNormalisation(
            tofPath,
            testData.alignmentTSVPath
        );

        std::string trafoFilePath;
        if (roundTrip) {
            trafoFilePath = "/tmp/";
            trafoFilePath += testData.name;
            trafoFilePath += (useRunA ? ".runA" : ".runB");
            trafoFilePath += ".trafoXML";
            rtNormalisation.saveToFile(exactTransformation, trafoFilePath);
        }
        else {
            trafoFilePath = useRunA
                            ? testData.runATrafoXML
                            : testData.runBTrafoXML;
        }
        ASSERT_TRUE(boost::filesystem::exists(trafoFilePath)) << trafoFilePath;

        auto irtTransformation = rtNormalisation.calculateNormalisationFromFile(
            trafoFilePath
        );
        compareTwoTransformations(exactTransformation, irtTransformation);
    }

    void compareTwoTransformations(
        const OpenMS::TransformationDescription &exactTransformation,
        const OpenMS::TransformationDescription &irtTransformation
    ) {
        const auto& exactParams = exactTransformation.getModelParameters();
        const auto& params = irtTransformation.getModelParameters();
        const double TOL = 1e-12;

        EXPECT_EQ(
            exactTransformation.getModelType(),
            irtTransformation.getModelType()
        );
        EXPECT_EQ(
            exactParams.getValue("symmetric_regression").toBool(),
            params.getValue("symmetric_regression").toBool()
        );
        EXPECT_NEAR(
            static_cast<double>(exactParams.getValue("span")),
            static_cast<double>(params.getValue("span")),
            TOL
        );
        EXPECT_NEAR(
            static_cast<double>(exactParams.getValue("slope")),
            static_cast<double>(params.getValue("slope")),
            TOL
        );
        EXPECT_NEAR(
            static_cast<double>(exactParams.getValue("intercept")),
            static_cast<double>(params.getValue("intercept")),
            TOL
        );
        EXPECT_EQ(
            static_cast<int>(exactParams.getValue("num_nodes")),
            static_cast<int>(params.getValue("num_nodes"))
        );
        for (const auto& val : {0.1, 1.0, 10.0, 100.0}) {
            EXPECT_NEAR(
                exactTransformation.apply(val),
                irtTransformation.apply(val),
                TOL
            ) << val;
        }
    }
};

TEST_F(RTNormalisationTest, testLoadRetentionTimeProCan90RunA) {
    bool useRunA = true;
    ProCan90TestData testData;
    testLoadRetentionTime(useRunA, testData);
}

TEST_F(RTNormalisationTest, testLoadRetentionTimeProCan90RunB) {
    bool useRunA = false;
    ProCan90TestData testData;
    testLoadRetentionTime(useRunA, testData);
}

TEST_F(RTNormalisationTest, testLoadRetentionTimeSGS2014RunA) {
    bool useRunA = true;
    SGS2014TestData testData;
    testLoadRetentionTime(useRunA, testData);
}

TEST_F(RTNormalisationTest, testLoadRetentionTimeSGS2014RunB) {
    bool useRunA = false;
    SGS2014TestData testData;
    testLoadRetentionTime(useRunA, testData);
}

TEST_F(RTNormalisationTest, testCalculateNormalisationFromFileProCan90RunA) {
    bool useRunA = true;
    ProCan90TestData testData;
    bool roundTrip = true;
    testCalculateNormalisationFromFile(useRunA, testData, roundTrip);
}

TEST_F(RTNormalisationTest, testCalculateNormalisationFromFileProCan90RunB) {
    bool useRunA = false;
    ProCan90TestData testData;
    bool roundTrip = true;
    testCalculateNormalisationFromFile(useRunA, testData, roundTrip);
}

TEST_F(RTNormalisationTest, testCalculateNormalisationFromFileSGS2014RunA) {
    bool useRunA = true;
    SGS2014TestData testData;
    bool roundTrip = true;
    testCalculateNormalisationFromFile(useRunA, testData, roundTrip);
}

TEST_F(RTNormalisationTest, testCalculateNormalisationFromFileSGS2014RunB) {
    bool useRunA = false;
    SGS2014TestData testData;
    bool roundTrip = true;
    testCalculateNormalisationFromFile(useRunA, testData, roundTrip);
}

TEST_F(RTNormalisationTest, testLoadNormalisationFromFileProCan90RunA) {
    bool useRunA = true;
    ProCan90TestData testData;
    bool roundTrip = false;
    testCalculateNormalisationFromFile(useRunA, testData, roundTrip);
}

TEST_F(RTNormalisationTest, testLoadNormalisationFromFileProCan90RunB) {
    bool useRunA = false;
    ProCan90TestData testData;
    bool roundTrip = false;
    testCalculateNormalisationFromFile(useRunA, testData, roundTrip);
}

TEST_F(RTNormalisationTest, testLoadNormalisationFromFileSGS2014RunA) {
    bool useRunA = true;
    SGS2014TestData testData;
    bool roundTrip = false;
    testCalculateNormalisationFromFile(useRunA, testData, roundTrip);
}

TEST_F(RTNormalisationTest, testLoadNormalisationFromFileSGS2014RunB) {
    bool useRunA = false;
    SGS2014TestData testData;
    bool roundTrip = false;
    testCalculateNormalisationFromFile(useRunA, testData, roundTrip);
}
}

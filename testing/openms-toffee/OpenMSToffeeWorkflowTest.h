/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <string>
#include <gtest/gtest.h>
#include <testing/openms-toffee/TestHelper.h>
#include <OpenMS/APPLICATIONS/TOPPBase.h>
#include <openms-toffee/OpenMSToffeeWorkflow.h>

namespace openmstoffee {

class OpenMSToffeeWorkflowTest :
    public ::testing::Test {
protected:
    // ==========================================
    // Test configuration
    // ==========================================

    enum Keys { Input, SRL, iRT, Output, RTNorm };
    using FileDict = std::map<Keys, std::string>;

    const FileDict good{
        // files which exist or can be created
        {Input, ProCan90TestData().runARetentionTimeTofPath},
        {SRL, ProCan90TestData().srlPQPPath},
        {iRT, ProCan90TestData().alignmentTSVPath},
        {Output, "/tmp/test_output.osw"},
        {RTNorm, ProCan90TestData().runATrafoXML}
    };

    const FileDict bad{
        // files which don't exist or can't be created
        {Input, "/bad/input"},
        {SRL, "/bad/srl"},
        {iRT, "/bad/irt"},
        {Output, "/bad/output"}
    };

    void runConfigTest(
        std::vector<std::string> vargs,
        OpenMS::TOPPBase::ExitCodes expectedCode
    );

    void
    add_arg(
        std::vector<std::string> &vargs,
        const std::string option
    ) const {
        vargs.push_back(option);
    }

    void
    add_arg(
        std::vector<std::string> &vargs,
        const std::string option,
        const std::string value
    ) const {
        vargs.push_back(option);
        vargs.push_back(value);
    }

    void
    add_program_name(
        std::vector<std::string> &vargs
    ) const {
        add_arg(vargs, "program name");
    }

    void
    add_log_arguments(
        std::vector<std::string> &vargs
    ) const {
        add_arg(vargs, "--log_arguments");
    }

    void
    add_input_file(
        std::vector<std::string> &vargs,
        const std::string &path
    ) const {
        add_arg(vargs, "-in", path);
    }

    void
    add_output_file(
        std::vector<std::string> &vargs,
        const std::string &path
    ) const {
        add_arg(vargs, "-out", path);
    }

    void
    add_srl_file(
        std::vector<std::string> &vargs,
        const std::string &path
    ) const {
        add_arg(vargs, "-tr", path);
    }

    void
    add_irt_file(
        std::vector<std::string> &vargs,
        const std::string &path
    ) const {
        add_arg(vargs, "-tr_irt", path);
    }

    std::vector<std::string>
    buildArgs_missing_parameters() const {
        std::vector<std::string> vargs;
        add_program_name(vargs);
        return vargs;
    }

    std::vector<std::string>
    buildArgs_log_arguments() const {
        std::vector<std::string> vargs;
        add_program_name(vargs);
        add_log_arguments(vargs);
        return vargs;
    }

    std::vector<std::string>
    buildArgs_file_arguments(
        FileDict in,
        FileDict srl,
        FileDict irt,
        FileDict out
    ) const {
        std::vector<std::string> vargs;
        add_program_name(vargs);
        add_log_arguments(vargs);
        add_input_file(vargs, in.at(Input));
        add_srl_file(vargs, srl.at(SRL));
        add_irt_file(vargs, irt.at(iRT));
        add_output_file(vargs, out.at(Output));
        return vargs;
    }

    std::vector<std::string>
    buildArgs_chromExtractParams() const {
        auto vargs = buildArgs_file_arguments(good, good, good, good);
        add_arg(vargs, "-rt_extraction_window", "666");
        add_arg(vargs, "-mz_extraction_window", "99");
        add_arg(vargs, "-mass_unit", "ppm");
        add_arg(vargs, "-irt_mz_extraction_window", "0.05");
        add_arg(vargs, "-irt_mass_unit", "thomson");
        return vargs;
    }

    std::vector<std::string>
    buildArgs_MassUnitFlagFailure() const {
        auto vargs = buildArgs_file_arguments(good, good, good, good);
        add_arg(vargs, "-irt_mass_unit", "SomeRandomText");
        return vargs;
    }

    // ==========================================
    // Test running the workflow
    // ==========================================
    template<typename TestData>
    void runOpenMSToffeeWorkflow(
        bool useRunA,
        bool usePQP,
        bool useFilteredSRL,
        TestData testData
    ) {
        std::string inPath;
        std::string srlPath;
        std::stringstream ssOutPath;
        ssOutPath << "/tmp/" << testData.name << (useRunA ? ".RunA" : ".RunB");
        if (useFilteredSRL) {
            ssOutPath << ".Filtered";
            inPath = useRunA ? testData.runARetentionTimeTofPath : testData.runBRetentionTimeTofPath;
            srlPath = usePQP ? testData.filteredSrlPQPPath : testData.filteredSrlTSVPath;
        }
        else {
            inPath = useRunA ? testData.runATofPath : testData.runBTofPath;
            srlPath = usePQP ? testData.srlPQPPath : testData.srlTSVPath;
        }
        ssOutPath << (usePQP ? ".osw" : ".tsv");

        std::vector<std::string> vargs;
        vargs.emplace_back("OpenMSToffeeWorkflow");
        vargs.emplace_back("-in");
        vargs.emplace_back(inPath);
        vargs.emplace_back("-tr");
        vargs.emplace_back(srlPath);
        vargs.emplace_back("-tr_irt");
        vargs.emplace_back(testData.alignmentTSVPath);
        vargs.emplace_back("-out");
        vargs.emplace_back(ssOutPath.str());

        // convert to argv
        std::vector<const char*> argv;
        argv.reserve(vargs.size());
        std::transform(
            vargs.cbegin(),
            vargs.cend(),
            std::back_inserter(argv),
            [](const auto& s) { return s.c_str(); }
        );
        auto argc = static_cast<int>(argv.size());

        OpenMSToffeeWorkflow wf;
        auto exitCode = wf.main(argc, argv.data());
        EXPECT_EQ(OpenMS::TOPPBase::ExitCodes::EXECUTION_OK, exitCode);
    }
};
}

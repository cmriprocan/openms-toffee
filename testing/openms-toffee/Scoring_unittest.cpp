/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include <gtest/gtest.h>
#include <boost/filesystem.hpp>
#include <testing/openms-toffee/TestHelper.h>
#include <openms-toffee/io_openms.h>
#include <openms-toffee/OpenMSToffeeWorkflow_impl/TransitionGroupScorer.h>
#include <openms-toffee/OpenMSToffeeWorkflow_impl/ToffeeScorer.h>
#include <openms-toffee/OpenMSToffeeWorkflow_impl/ToffeeSpectrumAccess.h>
#include <openms-toffee/OpenMSToffeeWorkflow_impl/TargetedExperimentOps.h>
#include <toffee/SwathRun.h>
#include <toffee/SwathMap.h>
#include <toffee/SwathMapSpectrumAccess.h>
#include <toffee/ToffeeWriter.h>

namespace openmstoffee {

class ScoringTestBase :
    public ::testing::Test {
protected:
    OpenMS::ChromExtractParams chromExtractParams;
    OpenMS::Param featureFinderParams;
    OpenSwath::LightTargetedExperiment inWindowTargetedExperiment;

    OpenMS::TransformationDescription worldToIRTTransformation;
    OpenMS::TransformationDescription irtToWorldTransformation;

    std::string tofName;
    toffee::MS2WindowDescriptor window;
    std::vector<OpenSwath::SwathMap> osSwathMaps;
    std::shared_ptr<const toffee::SwathRun> mSwathRun;
    std::shared_ptr<const toffee::SwathMap> ms1SwathMap;
    std::shared_ptr<const toffee::SwathMapSpectrumAccess> ms1SwathMapSpectrumAccess;
    std::shared_ptr<const toffee::SwathMap> ms2SwathMap;
    std::shared_ptr<const toffee::SwathMapSpectrumAccess> ms2SwathMapSpectrumAccess;

    OpenSwath::LightCompound peptide;
    OpenSwath::LightTransition transition;
    std::vector<OpenSwath::LightTransition> transitions;
    TargetedExperimentOps::PeptideIDMap peptideMap;
    TargetedExperimentOps::PeptideIDTransitionMap transitionMap;

    virtual std::string peptideSequence() const = 0;
    virtual std::string peptideId() const = 0;
    virtual std::string transitionId() const = 0;
    virtual void setUp(
        bool useRunA,
        bool forRTNormalisation,
        bool useMS1
    ) = 0;

    template<typename TestData>
    void baseSetUp(
        bool useRunA,
        bool forRTNormalisation,
        bool useMS1,
        std::string ms2Name,
        double ms2Lower,
        double ms2Upper,
        TestData testData
    ) {
        window.name = ms2Name;
        window.lower = ms2Lower;
        window.upper = ms2Upper;

        tofName = useRunA
                  ? testData.runARetentionTimeTofPath
                  : testData.runBRetentionTimeTofPath;

        ASSERT_TRUE(boost::filesystem::exists(tofName)) << tofName;
        mSwathRun = std::make_shared<const toffee::SwathRun>(tofName);
        if (useMS1) {
            ms1SwathMap = mSwathRun->loadSwathMapPtr(toffee::ToffeeWriter::MS1_NAME);
            ms1SwathMapSpectrumAccess = mSwathRun->loadSwathMapSpectrumAccessPtr(toffee::ToffeeWriter::MS1_NAME);
        }
        else {
            ms1SwathMap = nullptr;  // no MS1
            ms1SwathMapSpectrumAccess = nullptr;  // no MS1
        }

        ms2SwathMap = mSwathRun->loadSwathMapPtr(window.name);
        ms2SwathMapSpectrumAccess = mSwathRun->loadSwathMapSpectrumAccessPtr(window.name);

        if (ms1SwathMap) {
            osSwathMaps.emplace_back(
                ToffeeSpectrumAccess::toOpenSwathMap(ms1SwathMapSpectrumAccess, true)
            );
        }
        osSwathMaps.emplace_back(
            ToffeeSpectrumAccess::toOpenSwathMap(ms2SwathMapSpectrumAccess, false)
        );

        chromExtractParams = ChromatogramExtractor::defaultParams(forRTNormalisation);
        featureFinderParams = ToffeeScorer::defaultFeatureFinderParams(forRTNormalisation);
        if (useMS1) {
            featureFinderParams.setValue("Scores:use_ms1_correlation", "true");
            featureFinderParams.setValue("Scores:use_ms1_fullscan", "true");
        }

        ASSERT_TRUE(boost::filesystem::exists(testData.alignmentTSVPath)) << testData.alignmentTSVPath;
        auto lightAlignmentTransitions = TargetedExperimentOps::loadLightTSV(testData.alignmentTSVPath);
        inWindowTargetedExperiment = TargetedExperimentOps::filterToMassRange(
            lightAlignmentTransitions,
            window.lower,
            window.upper,
            chromExtractParams.min_upper_edge_dist
        );
        peptideMap = TargetedExperimentOps::mapPeptidesToIds(inWindowTargetedExperiment);
        transitionMap = TargetedExperimentOps::mapTransitionsToPeptideIds(inWindowTargetedExperiment);

        worldToIRTTransformation = useRunA
                                   ? testData.runATransformation()
                                   : testData.runBTransformation();
        irtToWorldTransformation = worldToIRTTransformation;
        irtToWorldTransformation.invert();

        ASSERT_NE(0, peptideMap.size());
        auto peptidePtr = peptideMap.at(peptideId());
        ASSERT_TRUE(peptidePtr);
        peptide = *peptidePtr;
        ASSERT_EQ(peptideSequence(), peptide.sequence);
        ASSERT_EQ(peptideId(), peptide.id);

        ASSERT_NE(0, transitionMap.size());
        transitions = transitionMap.at(peptideId());
        ASSERT_NE(0, transitions.size());
        transition = transitions.front();
        ASSERT_EQ(peptide.id, transition.peptide_ref);
        ASSERT_EQ(transitionId(), transition.transition_name);
    }

    void
    testExtractChromatogramRTNormalisation(
        bool useRunA,
        int expectedTotalIntensity
    ) {
        setUp(useRunA, true, false);
        auto tofRetentionTime = ms2SwathMap->retentionTime();
        ChromatogramExtractor extractor(chromExtractParams);
        auto chromatogram = extractor.extractChromatogram(
            *ms2SwathMap,
            peptide,
            transition,
            irtToWorldTransformation
        );
        EXPECT_EQ(tofRetentionTime.size(), chromatogram.size());
        int totalIntensity = 0;
        for (size_t i = 0; i < chromatogram.size(); ++i) {
            totalIntensity += chromatogram[i].getIntensity();
            EXPECT_NEAR(tofRetentionTime[i], chromatogram[i].getRT(), 1e-8)
                            << "@ i = "
                            << i
                            << ": "
                            << tofRetentionTime[i]
                            << " != "
                            << chromatogram[i].getRT();
        }
        // regression test
        EXPECT_EQ(expectedTotalIntensity, totalIntensity);
    }

    void
    testToffeeScorerCalculateScores(
        bool useRunA
    ) {
        bool useMS1{false};
        setUp(useRunA, false, useMS1);
        ToffeeScorer scorer;
        scorer.setToffeeFilePath(tofName, useMS1);
        scorer.updateChromExtractParams(chromExtractParams);
        scorer.updateFeatureFinderParams(featureFinderParams);
        scorer.setTargetedExperiment(inWindowTargetedExperiment);
        scorer.setWorldToIRTTransformation(worldToIRTTransformation);
        auto scores = scorer.calculateScores(window);
        EXPECT_EQ(peptideMap.size(), scores.size());
    }

    void
    testTransitionGroupScorerCalculate(
        bool useRunA,
        size_t expectedNumFeatures,
        double expectedRT,
        int expectedFeatureIntensity,
        int expectedFeatureFragmentIntensity,
        const std::map<std::string, double> &scoreRegressionParameters
    ) {
        setUp(useRunA, false, true);
        TransitionGroupScorer scorer(
            chromExtractParams,
            featureFinderParams,
            inWindowTargetedExperiment,
            worldToIRTTransformation,
            irtToWorldTransformation,
            osSwathMaps,
            ms1SwathMap,
            ms2SwathMap
        );
        auto score = scorer.calculate(peptide, transitions);
        EXPECT_EQ(peptide.id, score.id);
        EXPECT_EQ(peptide.id, score.peptide.id);
        EXPECT_FALSE(score.features.empty());
        ASSERT_EQ(transitions.size(), score.transitions.size());
        for (size_t i = 0; i < transitions.size(); ++i) {
            EXPECT_EQ(
                transitions[i].transition_name,
                score.transitions[i].transition_name
            );
        }

        // regression tests
        EXPECT_EQ(expectedNumFeatures, score.features.size());

        auto feature = *score.features.begin();
        double TOL = 1e-8;

        EXPECT_NEAR(expectedRT, feature.getRT(), TOL);
        EXPECT_EQ(0, expectedFeatureIntensity - feature.getIntensity())
            << feature.getIntensity();

        double REGRESSION_TOL = 1e-5;
        for (const auto &p : scoreRegressionParameters) {
            auto metaValue = feature.getMetaValue(p.first);
            EXPECT_NE(OpenMS::DataValue::EMPTY, metaValue) << p.first;
            if (metaValue != OpenMS::DataValue::EMPTY) {
#if 0
                std::cout << "scoreRegressionParameters[\"" << p.first << "\"] = " << std::setprecision(15) << static_cast<double>(metaValue) << ";" << std::endl;
#else
                EXPECT_NEAR(p.second, static_cast<double>(metaValue), REGRESSION_TOL) << p.first;
#endif
            }
        }

        auto featureFirstFragment = *feature.getSubordinates().begin();
        EXPECT_EQ(expectedFeatureFragmentIntensity, featureFirstFragment.getIntensity());
        EXPECT_EQ(transitionId(), featureFirstFragment.getMetaValue("native_id"));
    }
};

class ScoringTestProCan90 :
    public ScoringTestBase {
protected:

    std::string peptideSequence() const final { return std::string{"TPVISGGPYYER"}; }
    std::string peptideId() const final { return std::string{"TPVISGGPYYER_2"}; }
    std::string transitionId() const final { return std::string{"TPVISGGPYYER_2_y8_1"}; }

    void setUp(
        bool useRunA,
        bool forRTNormalisation,
        bool useMS1
    ) final {
        ScoringTestBase::baseSetUp(
            useRunA,
            forRTNormalisation,
            useMS1,
            "ms2-052",
            665.5,
            672.5,
            ProCan90TestData()
        );
    }
};

TEST_F(ScoringTestProCan90, extractChromatogramRTNormalisationRunA) {
    testExtractChromatogramRTNormalisation(true, 106992);
}

TEST_F(ScoringTestProCan90, extractChromatogramRTNormalisationRunB) {
    testExtractChromatogramRTNormalisation(false, 140694);
}

TEST_F(ScoringTestProCan90, testTransitionGroupScorerCalculateRunA) {
    bool useRunA = true;
    size_t expectedNumFeatures = 6;
    double expectedRT = 1469.6037069921208;
    int expectedFeatureIntensity = 239932;
    int expectedFeatureFragmentIntensity = 85908;

    std::map<std::string, double> scoreRegressionParameters;
    scoreRegressionParameters["assay_rt"] = 1438.4402123309;
    scoreRegressionParameters["delta_rt"] = 31.1634946612164;
    scoreRegressionParameters["initialPeakQuality"] = 0.971816269427428;
    scoreRegressionParameters["leftWidth"] = 1442.68798828125;
    scoreRegressionParameters["main_var_xx_swath_prelim_score"] = 5.28278192088646;
    scoreRegressionParameters["norm_RT"] = 28.3027200019623;
    scoreRegressionParameters["nr_peaks"] = 11;
    scoreRegressionParameters["peak_apices_sum"] = 32236;
    scoreRegressionParameters["rightWidth"] = 1505.76794433594;
    scoreRegressionParameters["rt_score"] = 0.492720001962315;
    scoreRegressionParameters["sn_ratio"] = 83.9045378392504;
    scoreRegressionParameters["total_xic"] = 269326;
    scoreRegressionParameters["var_bseries_score"] = 2;
    scoreRegressionParameters["var_dotprod_score"] = 0.843088293748659;
    scoreRegressionParameters["var_intensity_score"] = 0.890860889776702;
    scoreRegressionParameters["var_isotope_correlation_score"] = 0.994992623497773;
    scoreRegressionParameters["var_library_corr"] = 0.986306431750437;
    scoreRegressionParameters["var_library_dotprod"] = 0.989643822832557;
    scoreRegressionParameters["var_library_manhattan"] = 0.148969583593902;
    scoreRegressionParameters["var_library_rmsd"] = 0.0153469736729354;
    scoreRegressionParameters["var_library_rootmeansquare"] = 0.0213111922050857;
    scoreRegressionParameters["var_library_sangle"] = 0.136256151553095;
    scoreRegressionParameters["var_log_sn_score"] = 4.42967969829482;
    scoreRegressionParameters["var_manhatt_score"] = 0.535755720639394;
    scoreRegressionParameters["var_massdev_score"] = 5.66992769981334;
    scoreRegressionParameters["var_massdev_score_weighted"] = 3.93705697230901;
    scoreRegressionParameters["var_ms1_isotope_correlation"] = 0.998447088598701;
    scoreRegressionParameters["var_ms1_isotope_overlap"] = 0.3183919262474;
    scoreRegressionParameters["var_ms1_ppm_diff"] = 4.35380681504418;
    scoreRegressionParameters["var_ms1_xcorr_coelution"] = 1.17726130646056;
    scoreRegressionParameters["var_ms1_xcorr_shape"] = 0.883008926429338;
    scoreRegressionParameters["var_norm_rt_score"] = 0.00492720001962315;
    scoreRegressionParameters["var_xcorr_coelution"] = 1.48087789810624;
    scoreRegressionParameters["var_xcorr_coelution_weighted"] = 0.0489735463673474;
    scoreRegressionParameters["var_xcorr_shape"] = 0.832297386126406;
    scoreRegressionParameters["var_xcorr_shape_weighted"] = 0.960128860520814;
    scoreRegressionParameters["var_yseries_score"] = 8;

    testTransitionGroupScorerCalculate(
        useRunA,
        expectedNumFeatures,
        expectedRT,
        expectedFeatureIntensity,
        expectedFeatureFragmentIntensity,
        scoreRegressionParameters
    );
}

TEST_F(ScoringTestProCan90, testToffeeScorerCalculateScoresRunA) {
    testToffeeScorerCalculateScores(true);
}

TEST_F(ScoringTestProCan90, testToffeeScorerCalculateScoresRunB) {
    testToffeeScorerCalculateScores(false);
}

TEST_F(ScoringTestProCan90, testTransitionGroupScorerCalculateRunB) {
    bool useRunA = false;
    size_t expectedNumFeatures = 7;
    double expectedRT = 1781.3327678919213;
    int expectedFeatureIntensity = 293807;
    int expectedFeatureFragmentIntensity = 116503;

    std::map<std::string, double> scoreRegressionParameters;
    scoreRegressionParameters["assay_rt"] = 1680.67990623981;
    scoreRegressionParameters["delta_rt"] = 100.652861652114;
    scoreRegressionParameters["initialPeakQuality"] = 1.15828460524836;
    scoreRegressionParameters["leftWidth"] = 1760.07299804688;
    scoreRegressionParameters["main_var_xx_swath_prelim_score"] = 5.45103892309625;
    scoreRegressionParameters["norm_RT"] = 29.3521868117181;
    scoreRegressionParameters["nr_peaks"] = 11;
    scoreRegressionParameters["peak_apices_sum"] = 55558;
    scoreRegressionParameters["rightWidth"] = 1803.03796386719;
    scoreRegressionParameters["rt_score"] = 1.54218681171815;
    scoreRegressionParameters["sn_ratio"] = 91.3518474579656;
    scoreRegressionParameters["total_xic"] = 318899;
    scoreRegressionParameters["var_bseries_score"] = 2;
    scoreRegressionParameters["var_dotprod_score"] = 0.832189549099327;
    scoreRegressionParameters["var_intensity_score"] = 0.921316780548073;
    scoreRegressionParameters["var_isotope_correlation_score"] = 0.991641401308709;
    scoreRegressionParameters["var_library_corr"] = 0.977502095715934;
    scoreRegressionParameters["var_library_dotprod"] = 0.989288106063999;
    scoreRegressionParameters["var_library_manhattan"] = 0.139157839493527;
    scoreRegressionParameters["var_library_rmsd"] = 0.0176762054073102;
    scoreRegressionParameters["var_library_rootmeansquare"] = 0.0250407924246252;
    scoreRegressionParameters["var_library_sangle"] = 0.168259041974265;
    scoreRegressionParameters["var_log_sn_score"] = 4.51471850657097;
    scoreRegressionParameters["var_manhatt_score"] = 0.537466277184698;
    scoreRegressionParameters["var_massdev_score"] = 4.11302438683637;
    scoreRegressionParameters["var_massdev_score_weighted"] = 3.11024854118524;
    scoreRegressionParameters["var_ms1_isotope_correlation"] = 0.98600174045472;
    scoreRegressionParameters["var_ms1_isotope_overlap"] = 0.41809892067165;
    scoreRegressionParameters["var_ms1_ppm_diff"] = 3.61056029830382;
    scoreRegressionParameters["var_ms1_xcorr_coelution"] = 0;
    scoreRegressionParameters["var_ms1_xcorr_shape"] = 0.92595617867087;
    scoreRegressionParameters["var_norm_rt_score"] = 0.0154218681171815;
    scoreRegressionParameters["var_xcorr_coelution"] = 0.255349624138978;
    scoreRegressionParameters["var_xcorr_coelution_weighted"] = 0.00224230376628515;
    scoreRegressionParameters["var_xcorr_shape"] = 0.891224738655744;
    scoreRegressionParameters["var_xcorr_shape_weighted"] = 0.975514475665086;
    scoreRegressionParameters["var_yseries_score"] = 7;

    testTransitionGroupScorerCalculate(
        useRunA,
        expectedNumFeatures,
        expectedRT,
        expectedFeatureIntensity,
        expectedFeatureFragmentIntensity,
        scoreRegressionParameters
    );
}


class ScoringTestSGS2014 :
    public ScoringTestBase {
protected:

    std::string peptideSequence() const final { return std::string{"TPVITGAPYEYR"}; }
    std::string peptideId() const final { return std::string{"2"}; }
    std::string transitionId() const final { return std::string{"11"}; }

    void setUp(
        bool useRunA,
        bool forRTNormalisation,
        bool useMS1
    ) final {
        ScoringTestBase::baseSetUp(
            useRunA,
            forRTNormalisation,
            useMS1,
            "ms2-012",
            674.0,
            700.0,
            SGS2014TestData()
        );
    }
};

TEST_F(ScoringTestSGS2014, extractChromatogramRTNormalisationRunA) {
    testExtractChromatogramRTNormalisation(true, 399166);
}

TEST_F(ScoringTestSGS2014, extractChromatogramRTNormalisationRunB) {
    testExtractChromatogramRTNormalisation(false, 352391);
}

TEST_F(ScoringTestSGS2014, testToffeeScorerCalculateScoresRunA) {
    testToffeeScorerCalculateScores(true);
}

TEST_F(ScoringTestSGS2014, testToffeeScorerCalculateScoresRunB) {
    testToffeeScorerCalculateScores(false);
}

TEST_F(ScoringTestSGS2014, testTransitionGroupScorerCalculateRunA) {
    bool useRunA = true;
    size_t expectedNumFeatures = 1;
    double expectedRT = 3353.3370540505848;
    int expectedFeatureIntensity = 1469676;
    int expectedFeatureFragmentIntensity = 378500;

    std::map<std::string, double> scoreRegressionParameters;
    scoreRegressionParameters["assay_rt"] = 3338.18141070274;
    scoreRegressionParameters["delta_rt"] = 15.1556433478463;
    scoreRegressionParameters["initialPeakQuality"] = 1.24883635339856;
    scoreRegressionParameters["leftWidth"] = 3338.27294921875;
    scoreRegressionParameters["main_var_xx_swath_prelim_score"] = 5.49044202751219;
    scoreRegressionParameters["norm_RT"] = 34.0776240924458;
    scoreRegressionParameters["nr_peaks"] = 7;
    scoreRegressionParameters["peak_apices_sum"] = 461045;
    scoreRegressionParameters["rightWidth"] = 3369.10693359375;
    scoreRegressionParameters["rt_score"] = 0.446524092445813;
    scoreRegressionParameters["sn_ratio"] = 138.021643546198;
    scoreRegressionParameters["total_xic"] = 1512240;
    scoreRegressionParameters["var_bseries_score"] = 2;
    scoreRegressionParameters["var_dotprod_score"] = 0.864346110599176;
    scoreRegressionParameters["var_intensity_score"] = 0.971853674019997;
    scoreRegressionParameters["var_isotope_correlation_score"] = 0.99843360106168;
    scoreRegressionParameters["var_library_corr"] = 0.970421631746483;
    scoreRegressionParameters["var_library_dotprod"] = 0.997478920002616;
    scoreRegressionParameters["var_library_manhattan"] = 0.0598944676713627;
    scoreRegressionParameters["var_library_rmsd"] = 0.0136306368853443;
    scoreRegressionParameters["var_library_rootmeansquare"] = 0.0157469633971811;
    scoreRegressionParameters["var_library_sangle"] = 0.100440662729226;
    scoreRegressionParameters["var_log_sn_score"] = 4.92741051015081;
    scoreRegressionParameters["var_manhatt_score"] = 0.505767880456978;
    scoreRegressionParameters["var_massdev_score"] = 4.85263842436679;
    scoreRegressionParameters["var_massdev_score_weighted"] = 4.72353337594736;
    scoreRegressionParameters["var_ms1_isotope_correlation"] = 0.999454566749199;
    scoreRegressionParameters["var_ms1_isotope_overlap"] = 0;
    scoreRegressionParameters["var_ms1_ppm_diff"] = 2.18696788566542;
    scoreRegressionParameters["var_ms1_xcorr_coelution"] = 0;
    scoreRegressionParameters["var_ms1_xcorr_shape"] = 0.995652644931518;
    scoreRegressionParameters["var_norm_rt_score"] = 0.00446524092445813;
    scoreRegressionParameters["var_xcorr_coelution"] = 0;
    scoreRegressionParameters["var_xcorr_coelution_weighted"] = 0;
    scoreRegressionParameters["var_xcorr_shape"] = 0.998437728233747;
    scoreRegressionParameters["var_xcorr_shape_weighted"] = 0.997983017777961;
    scoreRegressionParameters["var_yseries_score"] = 5;

    testTransitionGroupScorerCalculate(
        useRunA,
        expectedNumFeatures,
        expectedRT,
        expectedFeatureIntensity,
        expectedFeatureFragmentIntensity,
        scoreRegressionParameters
    );
}

TEST_F(ScoringTestSGS2014, testTransitionGroupScorerCalculateRunB) {
    bool useRunA = false;
    size_t expectedNumFeatures = 3;
    double expectedRT = 3357.3603637415381;
    int expectedFeatureIntensity = 1361855;
    int expectedFeatureFragmentIntensity = 331354;

    std::map<std::string, double> scoreRegressionParameters;
    scoreRegressionParameters["assay_rt"] = 3349.98085838963;
    scoreRegressionParameters["delta_rt"] = 7.37950535191112;
    scoreRegressionParameters["initialPeakQuality"] = 1.24787393279417;
    scoreRegressionParameters["leftWidth"] = 3341.69702148438;
    scoreRegressionParameters["main_var_xx_swath_prelim_score"] = 5.43778172008586;
    scoreRegressionParameters["norm_RT"] = 33.8491186520931;
    scoreRegressionParameters["nr_peaks"] = 7;
    scoreRegressionParameters["peak_apices_sum"] = 354035;
    scoreRegressionParameters["rightWidth"] = 3375.95703125;
    scoreRegressionParameters["rt_score"] = 0.21801865209305;
    scoreRegressionParameters["sn_ratio"] = 100.709266763357;
    scoreRegressionParameters["total_xic"] = 1405241;
    scoreRegressionParameters["var_bseries_score"] = 2;
    scoreRegressionParameters["var_dotprod_score"] = 0.858893405676055;
    scoreRegressionParameters["var_intensity_score"] = 0.969125580594361;
    scoreRegressionParameters["var_isotope_correlation_score"] = 0.999444193942843;
    scoreRegressionParameters["var_library_corr"] = 0.974882216018202;
    scoreRegressionParameters["var_library_dotprod"] = 0.998248168144329;
    scoreRegressionParameters["var_library_manhattan"] = 0.0522245484153023;
    scoreRegressionParameters["var_library_rmsd"] = 0.0130675963243822;
    scoreRegressionParameters["var_library_rootmeansquare"] = 0.014326335000991;
    scoreRegressionParameters["var_library_sangle"] = 0.0915752530609109;
    scoreRegressionParameters["var_log_sn_score"] = 4.6122378189599;
    scoreRegressionParameters["var_manhatt_score"] = 0.519747854517075;
    scoreRegressionParameters["var_massdev_score"] = 3.40352571286726;
    scoreRegressionParameters["var_massdev_score_weighted"] = 3.37427211756359;
    scoreRegressionParameters["var_ms1_isotope_correlation"] = 0.998738677446444;
    scoreRegressionParameters["var_ms1_isotope_overlap"] = 0;
    scoreRegressionParameters["var_ms1_ppm_diff"] = 0.286620704829337;
    scoreRegressionParameters["var_ms1_xcorr_coelution"] = 0;
    scoreRegressionParameters["var_ms1_xcorr_shape"] = 0.995066042869567;
    scoreRegressionParameters["var_norm_rt_score"] = 0.00218018652093051;
    scoreRegressionParameters["var_xcorr_coelution"] = 0;
    scoreRegressionParameters["var_xcorr_coelution_weighted"] = 0;
    scoreRegressionParameters["var_xcorr_shape"] = 0.997025713097463;
    scoreRegressionParameters["var_xcorr_shape_weighted"] = 0.995765755609142;
    scoreRegressionParameters["var_yseries_score"] = 5;

    testTransitionGroupScorerCalculate(
        useRunA,
        expectedNumFeatures,
        expectedRT,
        expectedFeatureIntensity,
        expectedFeatureFragmentIntensity,
        scoreRegressionParameters
    );
}
}

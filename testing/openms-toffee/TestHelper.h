/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <string>
#include <OpenMS/ANALYSIS/MAPMATCHING/TransformationDescription.h>

namespace openmstoffee {
class TestHelper {
public:
    static std::string
    basename(
            const std::string &filepath
    );

    static bool
    filesAreEqual(
            const std::string& fileA,
            const std::string& fileB
    );
};

class ProCan90TestData {
public:
    ProCan90TestData();

    const std::string name; ///< name of the test set

    const std::string basePath; ///< input base directory
    const std::string runATofPath; ///< Raw data from ProCan90-M03-01.tof
    const std::string runBTofPath; ///< Raw data from ProCan90-M06-07.tof
    const std::string runARetentionTimeTofPath; ///< Raw data from ProCan90-M03-01.tof, filtered for iRT peptides
    const std::string runBRetentionTimeTofPath; ///< Raw data from ProCan90-M06-07.tof, filtered for iRT peptides
    const std::string srlTSVPath; ///< SRL file in text format, see README in this directory
    const std::string srlPQPPath; ///< SRL file in SQLITE format, see README in this directory
    const std::string alignmentTSVPath; ///< Alignment iRT SRL file in text format, see README in this directory
    const std::string alignmentPQPPath; ///< Alignment iRT SRL file in text format, see README in this directory

    const std::string filteredSrlTSVPath; ///< SRL file in text format filtered to include windows 10, 20, ..., 90
    const std::string filteredSrlPQPPath; ///< SRL file in SQLITE format filtered to include windows 10, 20, ..., 90

    // regression test files
    const std::string regressionBasePath; ///< regression results base directory
    const std::string testCalculateStringsForOutputSQLite;
    const std::string testCalculateStringsForOutputTSV;
    const std::string runATrafoXML; ///< Retention time normalisation for run A
    const std::string runBTrafoXML; ///< Retention time normalisation for run B

    OpenMS::TransformationDescription
    runATransformation();

    OpenMS::TransformationDescription
    runBTransformation();
};


class SGS2014TestData {
public:
    SGS2014TestData();

    const std::string name; ///< name of the test set

    const std::string basePath; ///< input base directory
    const std::string runATofPath; ///< Raw data from napedro_l120224_002_sw.tof
    const std::string runBTofPath; ///< Raw data from napedro_l120224_010_sw.tof
    const std::string runARetentionTimeTofPath; ///< Raw data from napedro_l120224_002_sw.tof, filtered for iRT peptides
    const std::string runBRetentionTimeTofPath; ///< Raw data from napedro_l120224_010_sw.tof, filtered for iRT peptides
    const std::string srlTSVPath; ///< SRL file in text format
    const std::string srlPQPPath; ///< SRL file in SQLITE format
    const std::string alignmentTSVPath; ///< Alignment iRT SRL file in text format
    const std::string alignmentPQPPath; ///< Alignment iRT SRL file in SQLITE format

    const std::string filteredSrlTSVPath; ///< Just the normal SRL, in text format
    const std::string filteredSrlPQPPath; ///< Just the normal SRL, in SQLITE format

    // regression test files
    const std::string runATrafoXML; ///< Retention time normalisation for run A
    const std::string runBTrafoXML; ///< Retention time normalisation for run B

    OpenMS::TransformationDescription
    runATransformation();

    OpenMS::TransformationDescription
    runBTransformation();
};
}

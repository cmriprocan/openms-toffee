"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

NOTE: Modified from:
https://github.com/pybind/cmake_example/blob/master/conda.recipe/meta.yaml
"""
import multiprocessing as mp
import os
import platform
import re
import subprocess
import sys
from distutils.version import LooseVersion

from setuptools import Extension, setup
from setuptools.command.build_ext import build_ext
from setuptools.command.install_lib import install_lib
try:
    from sphinx.setup_command import BuildDoc as BaseBuildDoc
    sphinx_enabled = True
except ModuleNotFoundError:
    sphinx_enabled = False


version_path = 'version'


def _get_version(include_patch=True):
    _version = None
    # check if we are on CircleCI to extract version from the environment
    # configured in .circleci/config.yml
    if os.environ.get('CI', False):
        _version = os.environ.get('NEW_GIT_TAG', None)
    if _version is None:
        with open(version_path, 'r') as f:
            _version = f.readlines()[-1].split(' ')[-1].strip()
    if include_patch:
        return _version
    else:
        return '.'.join(_version.split('.')[:2])


name = 'OpenMSToffee'
pysrc_dir = f'pysrc/{name}'
sourcedir = os.path.abspath('')
builddir = os.path.abspath('build')
version = _get_version(include_patch=False)
release = _get_version(include_patch=True)
executables = [
    'OpenMSToffeeWorkflow',
    'OpenMSToffee_unittest',
    'OpenMSToffee_regressiontest',
]
packages = [name]
package_dir = {name: pysrc_dir}

# Often, additional files need to be installed into a package. These files are often data that’s closely related to
# the package’s implementation, or text files containing documentation that might be of interest to programmers using
# the package. These files are called package data.
# Source: https://docs.python.org/3/distutils/setupscript.html
package_data = {
    name: [
        'unimod-accession-map_srep07896-s9.xls',
    ],
}

# The data_files option can be used to specify additional files needed by the module distribution: configuration files,
# message catalogs, data files, anything which doesn’t fit in the previous categories.
# Source: https://docs.python.org/3/distutils/setupscript.html
data_files = [
    'LICENSE',
]

entry_points = {
    'console_scripts': [
        'toffee_openms_rt_normalisation = OpenMSToffee.toffee_openms_rt_normalisation:main',
        'srl_peakview_to_openms = OpenMSToffee.srl_peakview_to_openms:main',
    ],
}


class CMakeExtension(Extension):
    def __init__(self, name):
        Extension.__init__(self, name, sources=[])


class CMakeBuild(build_ext):
    def run(self):
        try:
            out = subprocess.check_output(['cmake', '--version'])
        except OSError:
            raise RuntimeError(
                'CMake must be installed to build the following extensions: ' +
                ', '.join(e.name for e in self.extensions)
            )

        if platform.system() == 'Windows':
            cmake_version = LooseVersion(re.search(r'version\s*([\d.]+)', out.decode()).group(1))
            if cmake_version < '3.1.0':
                raise RuntimeError('CMake >= 3.1.0 is required on Windows')

        for ext in self.extensions:
            self.build_extension(ext)

    def build_extension(self, ext):
        extdir = os.path.abspath(os.path.dirname(self.get_ext_fullpath(ext.name)))
        self.build_temp = builddir

        cmake_args = [
            '-DCMAKE_LIBRARY_OUTPUT_DIRECTORY={}'.format(extdir),
            '-DCMAKE_RUNTIME_OUTPUT_DIRECTORY={}'.format(extdir),
            '-DPYTHON_EXECUTABLE=' + sys.executable
        ]

        cfg = 'Debug' if self.debug else 'RelWithDebInfo'
        build_args = ['--config', cfg]

        env = os.environ.copy()
        env['CXXFLAGS'] = '{} -DVERSION_INFO=\\"{}\\"'.format(
            env.get('CXXFLAGS', ''),
            self.distribution.get_version()
        )

        if platform.system() == 'Windows':
            cmake_args += ['-DCMAKE_LIBRARY_OUTPUT_DIRECTORY_{}={}'.format(cfg.upper(), extdir)]
            cmake_args += ['-DCMAKE_RUNTIME_OUTPUT_DIRECTORY_{}={}'.format(cfg.upper(), extdir)]
            if sys.maxsize > 2 ** 32:
                cmake_args += ['-A', 'x64']
            build_args += ['--', '/m']
        else:
            cmake_args += ['-DCMAKE_BUILD_TYPE=' + cfg]
            build_args += ['--', '-j{}'.format(mp.cpu_count())]
            env['CXXFLAGS'] = "{} -Wl,-rpath='$ORIGIN'".format(
                env.get('CXXFLAGS', '')
            )

        if not os.path.exists(self.build_temp):
            os.makedirs(self.build_temp)
        subprocess.check_call(['cmake', sourcedir] + cmake_args, cwd=self.build_temp, env=env)
        subprocess.check_call(['cmake', '--build', '.'] + build_args, cwd=self.build_temp)


class CMakeInstallLib(install_lib):
    def run(self):
        super().run()
        # create shell scripts that can be installed that points to main exe's
        if not self.distribution.scripts:
            self.distribution.scripts = []
        for exe in executables:
            fname = os.path.join(builddir, exe)
            with open(fname, 'w') as f:
                s = list()
                s.append('#!/bin/bash')
                s.append("LIB_DIR=`python -c 'import {0}; print({0}.__path__[0])'`")
                s.append('${{LIB_DIR}}/{1} "$@"')
                s.append('')
                f.write('\n'.join(s).format(name, exe))
            self.distribution.scripts.append(fname)


cmdclass = {
    'build_ext': CMakeBuild,
    'install_lib': CMakeInstallLib,
}
command_options = dict()


if sphinx_enabled:
    class BuildDoc(BaseBuildDoc):
        def run(self):
            try:
                subprocess.check_call(['doxygen', 'Doxyfile'], cwd='doc')
            except OSError:
                raise RuntimeError('Doxygen must be installed to created documentation')
            super(BuildDoc, self).run()

    cmdclass['build_sphinx'] = BuildDoc
    command_options['build_sphinx'] = {
        'project': ('setup.py', name),
        'version': ('setup.py', version),
        'release': ('setup.py', release),
        'source_dir': ('setup.py', 'doc'),
        'build_dir': ('setup.py', 'doc/_build/sphinx'),
    }


setup(
    name=name,
    version=release,
    author='ProCan Software Engineering',
    author_email='ProCanSofEngRobot@cmri.org.au',
    description='Wrapping OpenSwath for use with toffee',
    long_description='',
    python_requires='>=3.6',
    ext_modules=[
        CMakeExtension('OpenMSToffee.OpenMSToffee_python'),
    ],
    cmdclass=cmdclass,
    command_options=command_options,
    package_dir=package_dir,
    packages=packages,
    package_data=package_data,
    data_files=data_files,
    include_package_data=True,
    zip_safe=False,
    entry_points=entry_points,
    setup_requires=[
        'pytest-runner',
    ],
    tests_require=[
        'pytest',
    ],
    install_requires=[
        'numpy',
        'scipy',
    ],
)

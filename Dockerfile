# ---
# Base Image
FROM cmriprocan/openms:1.2.5 AS base

LABEL image=base

ENV TOFFEE_DIR=${INSTALL_PARENT_DIR}/toffee
ENV OPENMS_TOFFEE_DIR=${INSTALL_PARENT_DIR}/openms-toffee
ENV DIA_TEST_DATA_REPO=${INSTALL_PARENT_DIR}/dia-test-data

ARG bitbucket_user
ARG bitbucket_pw

USER root

RUN apt-get update \
 && apt-get -yq dist-upgrade \
 && apt-get install -yq --no-install-recommends \
        ninja-build \
        git \
        ssh \
        pkg-config \
        curl \
 && curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash \
 && apt-get install -yq --no-install-recommends \
        git-lfs \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

# install toffee -- including the annoying need to use pip, these versions *might* need to be
# bumped when toffee is bumped
RUN $CONDA_DIR/bin/conda install --quiet --yes -c cmriprocan -c plotly \
        "toffee=0.14.2" \
        "qt>=5.9" \
 && $CONDA_DIR/bin/conda clean -tipsy \
 && $CONDA_DIR/bin/python -m pip install \
        "psims==0.1.27" \
        "pyteomics==4.1" \
        "pyteomics.cythonize==0.2.2"

# build openms-toffee
RUN mkdir -p ${OPENMS_TOFFEE_DIR}
ADD . ${OPENMS_TOFFEE_DIR}
RUN cd ${OPENMS_TOFFEE_DIR} \
 && $CONDA_DIR/bin/python -m pip install -r pysrc/requirements.txt \
 && ls -lhF \
 && ls -lhF testing/openms-toffee/test_data


# ---
# Tests Image
FROM base AS test
ARG build

# Ensure there is only one RUN command after these LABELS. CircleCI looks at build container LABELS to identify which has
# the test results in it.
LABEL image=test
LABEL build=${build}

RUN git lfs clone \
         --include "ProCan90/tof/*.iRT.tof,ProCan90/srl/*,SwathGoldStandard/tof/*.iRT.tof,SwathGoldStandard/srl/*" \
         https://bitbucket.org/cmriprocan/dia-test-data ${DIA_TEST_DATA_REPO} \
 && ls -lhF ${DIA_TEST_DATA_REPO}/*/* \
 && cd ${OPENMS_TOFFEE_DIR} \
 && $CONDA_DIR/bin/python -m pip install . \
 && $CONDA_DIR/bin/python -m pip install pytest \
 && OpenMSToffee_unittest --gtest_output=xml:/test-reports/cpp-junit.xml \
 && $CONDA_DIR/bin/python -m pytest \
       --junitxml=/test-reports/py-junit.xml \
       --rootdir=`python -c "import OpenMSToffee as omt; print(list(omt.__path__)[0])"` \
       --import-mode=append \
       ${OPENMS_TOFFEE_DIR}/pysrc/tests


# ---
# Release Image
FROM base AS release

LABEL website="http://www.cmri.org.au/ProCan"
LABEL author="ProCan Software Engineering <ProCanSoftEngRobot@cmri.org.au>"

ARG NEW_GIT_TAG
ARG CI

LABEL image=release

# install and check to see if everything was installed correctly
RUN cd ${OPENMS_TOFFEE_DIR} \
 && $CONDA_DIR/bin/python -m pip install . \
 && cd ${OPENMS_TOFFEE_DIR}/.. \
 && rm -rf ${OPENMS_TOFFEE_DIR} \
 && echo "========================================" \
 && OpenMSToffeeWorkflow --help \
 && echo "========================================" \
 && $CONDA_DIR/bin/python -c "import OpenMSToffee as omt; print('version =', omt.__version__)" \
 && echo "========================================" \
 && toffee_openms_rt_normalisation --help \
 && echo "========================================" \
 && srl_peakview_to_openms --help \
 && echo "========================================" \
 && $CONDA_DIR/bin/python -c "import toffee; print('toffee version =', toffee.__version__)" \
 && echo "========================================" \
 && toffee_to_mzml --help \
 && echo "========================================" \
 && mzml_to_toffee --help

====================
OpenMSToffee: Python
====================

.. contents::
   :depth: 3

Working with `toffee` files
===========================


Calculating RT normalisation
----------------------------

.. code-block:: bash
   :linenos:

    $ toffee_openms_rt_normalisation --help
    usage: toffee_openms_rt_normalisation [-h]
                                          toffee_filename alignment_filename
                                          transformation_xml_filename

    Calculate the retention time normalisation for a toffee file using OpenMS as
    the wrapper

    positional arguments:
      toffee_filename       The output filename (*.tof)
      alignment_filename    The input alignment library (iRT) filename (*.tsv)
      transformation_xml_filename
                            The output transformation XML file (*.trafoXML)

    optional arguments:
      -h, --help            show this help message and exit


Converting SRLs
===============

.. code-block:: bash
   :linenos:

    $ srl_peakview_to_openms --help
    usage: srl_peakview_to_openms [-h] [-output_dir OUTPUT_DIR]
                                  [-minimum_number_of_transitions MINIMUM_NUMBER_OF_TRANSITIONS]
                                  [-maximum_number_of_transitions MAXIMUM_NUMBER_OF_TRANSITIONS]
                                  [-mz_cutoff MZ_CUTOFF]
                                  [-precursor_product_limit PRECURSOR_PRODUCT_LIMIT]
                                  [--drop_modifications] [--make_pqp]
                                  [--keep_intermediate_files] [--debug]
                                  sciex_filename

    Convert a Sciex ProteinPilot/PeakView/OneOmics SRL into a format that can be
    used by OpenMS

    positional arguments:
      sciex_filename        The input filename (*.txt)

    optional arguments:
      -h, --help            show this help message and exit
      -output_dir OUTPUT_DIR
                            The directory to save the output files. By default it
                            will be the same as the input file.
      -minimum_number_of_transitions MINIMUM_NUMBER_OF_TRANSITIONS
                            Define the minimum number of transitions for any PSM
      -maximum_number_of_transitions MAXIMUM_NUMBER_OF_TRANSITIONS
                            Define the maximum number of transitions for any PSM
      -mz_cutoff MZ_CUTOFF  Filter out any precursor ions with a mass over charge
                            below this threshold
      -precursor_product_limit PRECURSOR_PRODUCT_LIMIT
                            Filter out any product ions with a mass over charge
                            with this many Da of the precursor ion
      --drop_modifications  Remove all modifications from the final SRL
      --make_pqp            Create the new PQP sqlite format in addition the TSV
      --keep_intermediate_files
                            Remove intermediate files that were created
      --debug               Switch on more detailed logging


Internal Class Structure
========================

.. autofunction:: OpenMSToffee.log.set_stream_logger


.. autoclass:: OpenMSToffee.srl_peakview_to_openms.OpenSwathLibraryFromPeakview
   :members:
   :undoc-members:

"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import argparse
import logging
import os
import re
import subprocess
from collections import OrderedDict

# from log import set_stream_logger

import numpy as np

import pandas as pd

from .log import set_stream_logger

_LOGGER = logging.getLogger(__name__)


class OpenSwathLibraryFromPeakview(object):
    """
    Convert SRL file formats between PeakView and OpenMS. Furthermore, run the
    decoy generation on the OpenMS data once it has been generated
    """

    DROP_TEXT = 'DROP'
    PEAKVIEW_HEADERS = [
        'Q1',
        'Q3',
        'iRT',
        'stripped_sequence',
        'relative_intensity',
        'uniprot_id',
        'modification_sequence',
        'prec_z',
        'frg_type',
        'frg_z',
        'frg_nr'
    ]
    PEAKVIEW_RTCAL_PROTEIN = '[ RT-Cal protein ]'
    IRT_STRINGS = [
        'Bio',
        'RMIS',
        'ProCal',
    ]

    OPENSWATH_INDEX_COLS = ['TransitionGroupId']
    OPENSWATH_SORT_COLS = ['TransitionGroupId', 'LibraryIntensity']
    OPENSWATH_SORT_ASCENDING = [True, False]
    PV_TO_OS_COL_MAPPING = OrderedDict(
        Q1='PrecursorMz',
        Q3='ProductMz',
        iRT='NormalizedRetentionTime',
        TransitionId='TransitionId',
        CE='CollisionEnergy',
        relative_intensity='LibraryIntensity',
        TransitionGroupId='TransitionGroupId',
        ProteinId='ProteinId',
        PeptideGroupLabel='PeptideGroupLabel',
        stripped_sequence='PeptideSequence',
        modification_sequence='ModifiedPeptideSequence',
        uniprot_id='UniprotId',
        Decoy='Decoy',
        prec_z='PrecursorCharge',
        frg_type='FragmentType',
        frg_z='ProductCharge',
        frg_nr='FragmentSeriesNumber',
        Annotation='Annotation'
    )

    def __init__(self,
                 peakview_fname,
                 output_basename=None,
                 output_dir='',
                 minimum_number_of_transitions=3,
                 maximum_number_of_transitions=6,
                 mz_cutoff=400.0,
                 precursor_product_limit=10.0,
                 modifications_to_keep='CAM',
                 extra_irt_list=[],
                 make_pqp=False,
                 clean_up_files=True,
                 debug=True
                 ):
        """
        Convert a Sciex ProteinPilot/PeakView/OneOmics SRL into a format that can be used
        by OpenMS. Furthermore, using the TargetDecoyGenerator of OpenMS, calculate the
        library including decoys, using a reversed sequence

        :param peakview_fname: The input filename (*.txt)
        :param output_basename: The base of the output filenames, if None, this will be based
            off the peakview_fname
        :param output_dir: The directory to save the output files. By default it will be
            the same as the input file.
        :param minimum_number_of_transitions: Define the minimum number of transitions
            for any PSM
        :param maximum_number_of_transitions: Define the maximum number of transitions
            for any PSM
        :param mz_cutoff: Filter out any precursor ions with a mass over charge below
            this threshold
        :param precursor_product_limit: Filter out any product ions with a mass over charge
            with this many Da of the precursor ion
        :param modifications_to_keep: A comma separated string of modifications in Sciex format
            that should be retained. e.g. 'CAM,Oxi'
        :param make_pqp: Create the new PQP sqlite format in addition the TSV
        :param clean_up_files: Remove intermediate files that were created
        :param debug: Switch on more detailed logging
        """

        if debug and len(_LOGGER.handlers) == 0:
            set_stream_logger(level=logging.INFO)

        assert os.path.isfile(peakview_fname)
        if output_dir == '':
            output_dir = os.path.dirname(peakview_fname)

        if output_basename is None:
            output_basename = os.path.splitext(peakview_fname)[0]
        output_basename = os.path.basename(output_basename)

        self.peakview_fname = peakview_fname
        self.output_basename = output_basename
        self.output_dir = output_dir
        self.minimum_number_of_transitions = minimum_number_of_transitions
        self.maximum_number_of_transitions = maximum_number_of_transitions
        self.mz_cutoff = mz_cutoff
        self.precursor_product_limit = precursor_product_limit
        self.modifications_to_keep = modifications_to_keep.split(',')
        self.extra_irt_list = extra_irt_list
        self.make_pqp = make_pqp
        self.clean_up_files = clean_up_files
        self.debug = debug

    def convert(self):
        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)
        basename = os.path.join(self.output_dir, self.output_basename)

        # open and convert format
        df = self.open_peak_view()

        # split library from iRT
        df_library, df_irt = self.split_to_srl_and_alignment(df, self.extra_irt_list)
        df_irt.to_csv(os.path.join(self.output_dir, 'df_irt'), sep='\t')

        self._convert_irt(
            df_irt,
            basename
        )
        self._convert_library(
            df_library,
            basename
        )

    def _convert_library(self, df_library, basename):

        if df_library.shape[0] == 0:
            _LOGGER.warning('SRL library has no entries!')
            return

        # convert to openswath
        df_library = self.convert_peakview_to_openswath(df_library)

        # sort and filter
        df_library = self.filter_and_normalise_openswath(df_library)

        # filter out the number of transitions
        if self.maximum_number_of_transitions > 0:
            grouped = df_library.groupby(level=[0])
            df_library = grouped.head(n=self.maximum_number_of_transitions)
        df_library = self.remove_minimum_fragments(df_library)

        # create decoys and save
        self.create_decoys(df_library, basename)

    def _convert_irt(self, df_irt, basename):

        if df_irt.shape[0] == 0:
            _LOGGER.warning('iRT library has no entries!')
            return

        # convert to openswath
        df_irt = self.convert_peakview_to_openswath(df_irt)

        # sort and filter
        df_irt = self.filter_and_normalise_openswath(df_irt)
        df_irt = self.remove_minimum_fragments(df_irt)

        # save to file
        self.create_calibration(df_irt, basename)

    def open_peak_view(self):
        """Internal class method, exposed only for testing"""
        _LOGGER.debug('Reading: {}'.format(self.peakview_fname))
        df = pd.read_csv(self.peakview_fname, sep='\t', engine='c')
        # be robust against lower case
        df.rename(columns={'q1': 'Q1', 'q3': 'Q3', 'irt': 'iRT'}, inplace=True)
        err = []
        for c in self.PEAKVIEW_HEADERS:
            if c not in df.columns:
                err.append(f'\t- {c}')
        if len(err) > 0:
            raise ValueError('PeakView input file missing the following headers:\n' + '\n'.join(err))

        # drop anything with a null intensity
        mask1 = ~df.relative_intensity.isnull()

        # drop the reverse (decoy) proteins that may have slipped through
        # from the ProteinPilot searching
        mask2 = ~df['uniprot_id'].str.contains('RRRRR')

        return df.loc[mask1 & mask2, self.PEAKVIEW_HEADERS].copy()

    @classmethod
    def split_to_srl_and_alignment(cls, df, extra_irt_list=[]):
        """Internal class method, exposed only for testing"""
        rt_protein_mask = df['uniprot_id'] == cls.PEAKVIEW_RTCAL_PROTEIN
        df_library = df[~rt_protein_mask].copy()

        mask = rt_protein_mask
        for irt_string in cls.IRT_STRINGS:
            mask |= df['uniprot_id'].str.contains(irt_string)
        for irt_string in extra_irt_list:
            mask |= df['stripped_sequence'] == irt_string
        df_irt = df[mask].copy()

        # drop the duplicate user defined '[ RT-Cal protein ]' PSMs -- these will have their
        # normal defined uniprot_id and the additional RT-cal -- drop the latter
        duplicate_mask = df_irt.groupby('Q1')['uniprot_id'].nunique() > 1
        duplicate_q1_vals = duplicate_mask[duplicate_mask].index.tolist()
        duplicate_mask = ~(df_irt.Q1.isin(duplicate_q1_vals)
                           & (df_irt['uniprot_id'] == cls.PEAKVIEW_RTCAL_PROTEIN))
        df_irt = df_irt[duplicate_mask].copy()

        return df_library, df_irt

    def convert_peakview_to_openswath(self, df):
        """Internal class method, exposed only for testing"""
        _LOGGER.debug('Converting from peakview to openswath')
        assert set(df.columns) == set(self.PEAKVIEW_HEADERS)
        assert df.shape[0] > 0

        # fix modifications
        _LOGGER.debug('Fixing the modifications')
        df['modification_sequence'] = self.rename_modifications(df['modification_sequence'])
        df['stripped_sequence'] = self.rename_modifications(df['stripped_sequence'])
        df = df[(df['modification_sequence'] != self.DROP_TEXT) &
                (df['stripped_sequence'] != self.DROP_TEXT)].copy()
        assert df.shape[0] > 0

        # filter minimum number of transitions
        _LOGGER.debug('Filtering minimum number of transitions')
        df.sort_values(
            ['modification_sequence', 'prec_z', 'Q3'],
            ascending=True,
            inplace=True
        )
        df.set_index(['modification_sequence', 'prec_z'], drop=False, inplace=True)

        # add missing columns
        _LOGGER.debug('Adding the missing columns')
        df['CollisionEnergy'] = -1
        df['PeptideGroupLabel'] = 'light'
        df['Decoy'] = 0
        df['Annotation'] = df['frg_type'].map(str) + df['frg_nr'].map(str)
        df['ProteinId'] = df['uniprot_id']

        # update the TransitionGroupId with fragment counts
        _LOGGER.debug('Updating TransitionGroupId')
        df['TransitionGroupId'] = (
            df['modification_sequence'] +
            '_' +
            df['prec_z'].map(str)
        )
        df['PeptideGroupLabel'] = df['TransitionGroupId']

        # use an integer counter to ensure that all transitions are unique
        _LOGGER.debug('Updating TransitionId')
        df['TransitionId'] = (
            df['TransitionGroupId'] +
            '_' +
            df['Annotation'] +
            '_' +
            df['frg_z'].map(str)
        )

        # drop duplicate Ids -- this comes from the situation where the same
        # peptide might correspond to multiple proteins. This should have been
        # caught upstream, but this is a double check
        df.drop_duplicates(subset='TransitionId', keep=False, inplace=True)

        # define the headers in OpenSWATH terms
        df.rename(columns=self.PV_TO_OS_COL_MAPPING, inplace=True)

        # reorder the columns
        df.reindex(self.PV_TO_OS_COL_MAPPING.values(), axis=1, copy=False)

        # set the index
        return df.set_index(self.OPENSWATH_INDEX_COLS, drop=False, inplace=False)

    def filter_and_normalise_openswath(self, df):
        """Internal class method, exposed only for testing"""
        _LOGGER.debug('Filtering and normalising openswath')
        intensity_name = 'LibraryIntensity'

        # Filter transitions by m/z and around precursor
        df = df[(df['ProductMz'] > self.mz_cutoff) &
                ((df['ProductMz'] < df['PrecursorMz'] - self.precursor_product_limit) |
                 (df['ProductMz'] > df['PrecursorMz'] + self.precursor_product_limit))].copy()

        # normalise the intensity for each group
        assert df.index.duplicated
        grouped = df[intensity_name].groupby(level=[0])
        max_intensity_for_transition = grouped.agg(np.max).to_dict()
        df['max_intensity'] = df.TransitionGroupId.map(max_intensity_for_transition)
        df[intensity_name] = 10000.0 * df[intensity_name] / df['max_intensity']
        df.drop('max_intensity', axis=1, inplace=True)

        # re-sort the data
        df.reset_index(drop=True, inplace=True)
        df.sort_values(self.OPENSWATH_SORT_COLS, ascending=self.OPENSWATH_SORT_ASCENDING, inplace=True)
        df.set_index(self.OPENSWATH_INDEX_COLS, drop=False, inplace=True)
        return df

    def remove_minimum_fragments(self, df):
        """Internal class method, exposed only for testing"""
        _LOGGER.debug('Removing minimum fragments')
        # filter the min number of transitions
        grouped = df['TransitionGroupId'].groupby(level=[0])
        mask = (grouped.size() >= self.minimum_number_of_transitions).reindex(df.index, copy=True)
        df = df[mask].reindex(copy=True)
        assert (df['TransitionGroupId'].groupby(level=[0]).size() >= self.minimum_number_of_transitions).all()
        return df

    def create_decoys(self, df_library, basename):
        """Internal class method, exposed only for testing"""
        _LOGGER.debug('Creating decoys')

        # ---
        # save to files

        lib_basename = '{}.OpenSwath'.format(basename)
        library_fname = '{}.tsv'.format(lib_basename)
        library_traml_fname = '{}.TraML'.format(lib_basename)

        decoy_basename = '{}.OpenSwath.reverse_decoys'.format(basename)
        library_traml_decoy_fname = '{}.TraML'.format(decoy_basename)

        # save raw file
        _LOGGER.debug('Saving: {}'.format(library_fname))
        df_library.to_csv(library_fname, sep=str('\t'), index=False)  # TODO: the str() here is to fix py2 py3 issue

        # convert to TraML
        cmd = list()
        cmd.append('TargetedFileConverter')
        cmd.extend(('-in', library_fname))
        cmd.extend(('-out', library_traml_fname))
        cmd = ' '.join(cmd)
        try:
            _LOGGER.debug('Starting {}'.format(cmd))
            result = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
            _LOGGER.info(result)
        except subprocess.CalledProcessError as e:
            _LOGGER.error(e.output)
            _LOGGER.error(str(e))
            raise

        # create decoys
        cmd = list()
        cmd.append('OpenSwathDecoyGenerator')
        cmd.extend(('-in', library_traml_fname))
        cmd.extend(('-out', library_traml_decoy_fname))
        cmd.extend(('-method', 'reverse'))
        cmd = ' '.join(cmd)
        try:
            _LOGGER.debug('Starting {}'.format(cmd))
            result = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
            _LOGGER.info(result)
        except subprocess.CalledProcessError as e:
            _LOGGER.error(e.output)
            _LOGGER.error(str(e))
            raise

        # convert decoys back to TSV
        out_types = list()
        out_types.append('tsv')
        if self.make_pqp:
            out_types.append('pqp')
        for out_type in out_types:
            library_decoy_fname = '{}.{}'.format(decoy_basename, out_type)
            cmd = list()
            cmd.append('TargetedFileConverter')
            cmd.extend(('-in', library_traml_decoy_fname))
            cmd.extend(('-out', library_decoy_fname))
            cmd = ' '.join(cmd)
            try:
                _LOGGER.debug('Starting {}'.format(cmd))
                result = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
                _LOGGER.info(result)
            except subprocess.CalledProcessError as e:
                _LOGGER.error(e.output)
                _LOGGER.error(str(e))
                raise

        if self.clean_up_files:
            os.remove(library_traml_fname)
            os.remove(library_traml_decoy_fname)

    @classmethod
    def create_calibration(cls, df_irt, basename):
        """Internal class method, exposed only for testing"""
        _LOGGER.debug('Creating calibration')

        irt_basename = '{}.OpenSwath.iRT'.format(basename)
        irt_fname = '{}.tsv'.format(irt_basename)
        irt_traml_fname = '{}.TraML'.format(irt_basename)

        # ---
        # filter the RT calibration peptides
        _LOGGER.debug('Saving: {}'.format(irt_fname))
        df_irt.to_csv(irt_fname, sep=str('\t'), index=False)  # TODO: the str() here is to fix py2 py3 issue

        # convert to TraML
        cmd = list()
        cmd.append('TargetedFileConverter')
        cmd.extend(('-in', irt_fname))
        cmd.extend(('-out', irt_traml_fname))
        cmd = ' '.join(cmd)
        try:
            _LOGGER.debug('Starting {}'.format(cmd))
            result = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
            _LOGGER.info(result)
        except subprocess.CalledProcessError as e:
            _LOGGER.error(e.output)
            _LOGGER.error(str(e))
            raise

        # convert to back to tsv
        cmd = list()
        cmd.append('TargetedFileConverter')
        cmd.extend(('-in', irt_traml_fname))
        cmd.extend(('-out', irt_fname))
        cmd = ' '.join(cmd)
        try:
            _LOGGER.debug('Starting {}'.format(cmd))
            result = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
            _LOGGER.info(result)
        except subprocess.CalledProcessError as e:
            _LOGGER.error(e.output)
            _LOGGER.error(str(e))
            raise

    def rename_modifications(self, mod_peptide_col):
        """
        Internal class method, exposed only for testing.
        Convert the modification format of PeakView into UniMod format
        """
        missed_matches = set()
        accession_unimod = self._get_accession_unimod_map()

        def find_replace(row):
            # use a look ahead to get the modification site as well
            matches = re.findall('([A-Z])\[(.*?)\]', row)  # NOQA W605
            for m in matches:
                assert len(m) == 2, str(m)
                search = '{}[{}]'.format(*m)
                try:
                    # get unimod accession number
                    row = row.replace(search, accession_unimod[search])
                except KeyError:
                    missed_matches.add(search)
                    return self.DROP_TEXT
            # remove any hyphens
            if '-' in row:
                return self.DROP_TEXT
            return row

        result = mod_peptide_col.apply(find_replace)
        if len(missed_matches) > 0:
            _LOGGER.debug("Couldn't find unimod value for: {}".format(', '.join(missed_matches)))
        return result

    def _get_accession_unimod_map(self):
        """
        Accession numbers come from:
        www.nature.com/article-assets/npg/srep/2015/150120/srep07896/extref/srep07896-s9.xls
        """
        scipt_file_dir = os.path.dirname(os.path.abspath(__file__))
        accession_fname = os.path.join(scipt_file_dir, 'unimod-accession-map_srep07896-s9.xls')

        # read the excel document
        accession_unimod = pd.read_excel(accession_fname)

        # drop unverified mods
        accession_unimod = accession_unimod[accession_unimod['UniMod: Verified'] == 'Yes']

        # get the columns we care about
        unimod_cols = ['Site', 'TLC in ProteinPilot Software', 'UniMod: Accession #']
        accession_unimod = accession_unimod[unimod_cols].dropna()

        # filter out the sites that have '-term' in them
        accession_unimod = accession_unimod[accession_unimod['Site'].apply(lambda x: 'term' not in x)]

        # Add modifications not in the original list -- those commented out give errors downstream
        new_unimods = list()
        # Oxidisation
        new_unimods.append(['M', 'Oxi', 35])
        new_unimods.append(['W', 'Oxi', 35])
        new_unimods.append(['H', 'Oxi', 35])
        new_unimods.append(['D', 'Oxi', 35])
        new_unimods.append(['K', 'Oxi', 35])
        new_unimods.append(['Y', 'Oxi', 35])
        new_unimods.append(['R', 'Oxi', 35])
        new_unimods.append(['N', 'Oxi', 35])
        new_unimods.append(['P', 'Oxi', 35])
        new_unimods.append(['C', 'Oxi', 35])
        # Carbamyl
        new_unimods.append(['N', 'CRM', 5])
        # S-carbamoylmethylcysteine cyclization (N-terminus)
        new_unimods.append(['C', 'PCm', 26])
        # other from msproteomicstools
        new_unimods.append(['C', 'CAM', 4])
        new_unimods.append(['K', '+08', 259])
        new_unimods.append(['R', '+10', 267])
        new_unimods.append(['E', 'PGE', 27])
        new_unimods.append(['Q', 'PGQ', 28])
        new_unimods.append(['S', 'Pho', 21])
        new_unimods.append(['T', 'Pho', 21])
        new_unimods.append(['Y', 'Pho', 21])
        new_unimods.append(['N', 'Dea', 7])
        new_unimods.append(['Q', 'Dea', 7])
        new_unimods.append(['C', 'XXX', 39])
        # new_unimods.append(['Oxi', 35])
        # new_unimods.append(['PCm', 26])
        # new_unimods.append(['MDe', 528])  # Deamidation followed by a methylation
        # new_unimods.append(['HKy', 350])  # tryptophan oxidation to hydroxykynurenin
        # # new_unimods.append(['Dea', 7])  # Deamidation (should already be in the list)
        # new_unimods.append(['dAm', 385])  # Ammonia-loss
        # # new_unimods.append(['2CM', 1290])  # Dicarbamidomethyl (this gives errors with the obo from OpenSwath)
        # # new_unimods.append(['Dhy', 23])  # Dehydrated (should already be in the list)
        # # new_unimods.append(['+1K', 1301])  # Addition of lysine
        # new_unimods.append(['CRM', 5])  # Carbamyl
        # # new_unimods.append(['-1R', 1287])  # Loss of arginine
        # # new_unimods.append(['+1R', 1288])  # Addition of arginine

        accession_unimod = accession_unimod.append(pd.DataFrame(new_unimods, columns=accession_unimod.columns))

        # ensure that all of the sites are uppercase
        accession_unimod['Site'] = accession_unimod['Site'].map(lambda x: x.upper())

        # drop duplicates
        accession_unimod = accession_unimod.drop_duplicates()

        # drop codes that appear to give errors
        accession_unimod = accession_unimod[accession_unimod['TLC in ProteinPilot Software'] != '-1K']
        accession_unimod = accession_unimod[accession_unimod['TLC in ProteinPilot Software'] != 'PGE']

        # only keep mods specified by the user
        mask = accession_unimod['TLC in ProteinPilot Software'].isin(self.modifications_to_keep)
        accession_unimod = accession_unimod[mask]

        # define the search and replace text
        accession_unimod['search'] = (accession_unimod['Site'] +
                                      '[' +
                                      accession_unimod['TLC in ProteinPilot Software'] +
                                      ']')
        accession_unimod['replace'] = accession_unimod.apply(
            lambda x: '{}(UniMod:{})'.format(x['Site'], int(x['UniMod: Accession #'])),
            axis=1
        )

        # convert to a dictionary for faster lookup
        accession_unimod.set_index('search', inplace=True, drop=True)
        accession_unimod = accession_unimod['replace'].to_dict()
        return accession_unimod


def get_extra_irt_peptides(args):
    if args.extra_irt_file is None:
        result = []
    else:
        result = [line.strip() for line in args.extra_irt_file.readlines()]

    if args.extra_irt_peptide is not None:
        result.extend(args.extra_irt_peptide)

    return result


def main():
    parser = argparse.ArgumentParser(
        description=('Convert a Sciex ProteinPilot/PeakView/OneOmics SRL '
                     'into a format that can be used by OpenMS')
    )
    parser.add_argument(
        'sciex_filename',
        type=str,
        help='The input filename (*.txt)'
    )
    parser.add_argument(
        '-output_dir',
        default='',
        type=str,
        help=('The directory to save the output files. By default it will be '
              'the same as the input file.')
    )
    parser.add_argument(
        '-minimum_number_of_transitions',
        default=3,
        type=float,
        help='Define the minimum number of transitions for any PSM'
    )
    parser.add_argument(
        '-maximum_number_of_transitions',
        default=6,
        type=float,
        help='Define the maximum number of transitions for any PSM'
    )
    parser.add_argument(
        '-mz_cutoff',
        default=400.0,
        type=float,
        help=('Filter out any precursor ions with a mass over charge below '
              'this threshold')
    )
    parser.add_argument(
        '-precursor_product_limit',
        default=10.0,
        type=float,
        help=('Filter out any product ions with a mass over charge '
              'with this many Da of the precursor ion')
    )
    parser.add_argument(
        '-modifications_to_keep',
        default='CAM',
        type=str,
        help=('A comma separated string of modifications in Sciex format'
              "that should be retained. e.g. 'CAM,Oxi'")
    )
    parser.add_argument(
        '--make_pqp',
        default=False,
        action='store_true',
        help='Create the new PQP sqlite format in addition the TSV'
    )
    parser.add_argument(
        '--keep_intermediate_files',
        default=False,
        action='store_true',
        help='Remove intermediate files that were created'
    )
    parser.add_argument(
        '--debug',
        default=False,
        action='store_true',
        help='Switch on more detailed logging'
    )
    parser.add_argument(
        '--extra-irt-file',
        type=argparse.FileType('r'),
        help='A file with a list of extra peptides to include in the iRT list (file should be one peptide per line)',
    )
    parser.add_argument(
        '-e', '--extra-irt-peptide',
        type=str,
        action='append',
        help='The name of an extra peptide to include in the iRT list (can be specified multiple times)',
    )

    args = parser.parse_args()
    extra_irt_list = get_extra_irt_peptides(args)

    converter = OpenSwathLibraryFromPeakview(
        peakview_fname=args.sciex_filename,
        output_dir=args.output_dir,
        minimum_number_of_transitions=args.minimum_number_of_transitions,
        maximum_number_of_transitions=args.maximum_number_of_transitions,
        mz_cutoff=args.mz_cutoff,
        precursor_product_limit=args.precursor_product_limit,
        modifications_to_keep=args.modifications_to_keep,
        extra_irt_list=extra_irt_list,
        make_pqp=args.make_pqp,
        clean_up_files=not args.keep_intermediate_files,
        debug=args.debug
    )
    converter.convert()


if __name__ == '__main__':
    main()

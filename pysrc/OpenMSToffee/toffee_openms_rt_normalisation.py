"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import argparse
import os

from OpenMSToffee import RTNormalisation


def main():
    parser = argparse.ArgumentParser(
        description='Calculate the retention time normalisation for '
                    'a toffee file using OpenMS as the wrapper'
    )
    parser.add_argument(
        'toffee_filename',
        type=str,
        help='The output filename (*.tof)'
    )
    parser.add_argument(
        'alignment_filename',
        type=str,
        help='The input alignment library (iRT) filename (*.tsv)'
    )
    parser.add_argument(
        'transformation_xml_filename',
        type=str,
        help='The output transformation XML file (*.trafoXML)'
    )
    args = parser.parse_args()
    assert os.path.isfile(args.toffee_filename)
    normaliser = RTNormalisation(
        args.toffee_filename,
        args.alignment_filename
    )
    normaliser.calculateAndSaveToFile(
        args.transformation_xml_filename
    )


if __name__ == '__main__':
    main()

"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import os
import unittest

from OpenMSToffee.srl_peakview_to_openms import OpenSwathLibraryFromPeakview

import pandas as pd


class TestOpenSwathLibraryFromPeakview(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.data_dir = os.environ.get('DIA_TEST_DATA_REPO', None)
        assert cls.data_dir is not None
        cls.hek_sciex_fname = os.path.join(cls.data_dir, 'ProCan90', 'srl', 'hek_srl.sciex.txt')

    def test_load_peak_view(self):
        mz_cutoff, precursor_product_limit = 400, 10
        converter = OpenSwathLibraryFromPeakview(
            self.hek_sciex_fname,
            mz_cutoff=mz_cutoff,
            precursor_product_limit=precursor_product_limit,
            modifications_to_keep='CAM,Oxi'
        )

        df = converter.open_peak_view()
        self.assertCountEqual(
            converter.PEAKVIEW_HEADERS,
            df.columns
        )

        # no null intensities
        self.assertFalse(df.relative_intensity.isnull().any())

        # remove the reverse peptides
        self.assertFalse(df['uniprot_id'].str.contains('RRRRR').any())

        df_library, df_irt = converter.split_to_srl_and_alignment(df)

        # regression test
        self.assertEqual(1708536, df_library.shape[0])
        self.assertEqual(1814, df_irt.shape[0])

        # ensure there are not RT cal proteins in the main library
        cal_txt = converter.PEAKVIEW_RTCAL_PROTEIN
        self.assertFalse(df_library.uniprot_id.str.contains(cal_txt, regex=False).any())

        # disallow duplicate entries in alignment
        duplicate_mask = df_irt.groupby('Q1')['uniprot_id'].nunique() > 1
        if duplicate_mask.any():
            df1 = df_irt.set_index('Q1')
            print(df1.loc[duplicate_mask[duplicate_mask].index])  # NOQA T001
        self.assertFalse(duplicate_mask.any())

        # convert to openswath
        df_irt = converter.convert_peakview_to_openswath(df_irt)
        self.assertCountEqual(
            converter.PV_TO_OS_COL_MAPPING.values(),
            df_irt.columns
        )
        self.assertEqual(673, df_irt.shape[0])
        first = df_irt.iloc[0]
        self.assertEqual('sw|RMIS|IS_protein', first.UniprotId)
        self.assertEqual('sw|RMIS|IS_protein', first.ProteinId)
        self.assertEqual('AAEESVGTMGNR', first.PeptideSequence)
        self.assertEqual('AAEESVGTM(UniMod:35)GNR', first.ModifiedPeptideSequence)
        self.assertEqual('AAEESVGTM(UniMod:35)GNR_2', first.TransitionGroupId)
        self.assertEqual('AAEESVGTM(UniMod:35)GNR_2_b2_1', first.TransitionId)
        self.assertEqual('b2', first.Annotation)
        self.assertEqual('b', first.FragmentType)
        self.assertEqual(2, first.FragmentSeriesNumber)
        self.assertEqual(2, first.PrecursorCharge)
        self.assertEqual(1, first.ProductCharge)

        # convert to openswath
        df_library = converter.convert_peakview_to_openswath(df_library)
        self.assertCountEqual(
            converter.PV_TO_OS_COL_MAPPING.values(),
            df_library.columns
        )
        self.assertEqual(1389518, df_library.shape[0])
        first = df_library.iloc[0]
        self.assertEqual('sp|P55011|S12A2_HUMAN', first.UniprotId)
        self.assertEqual('sp|P55011|S12A2_HUMAN', first.ProteinId)
        self.assertEqual('AAAAAAAAAAAAAAAGAGAGAK', first.PeptideSequence)
        self.assertEqual('AAAAAAAAAAAAAAAGAGAGAK', first.ModifiedPeptideSequence)
        self.assertEqual('AAAAAAAAAAAAAAAGAGAGAK_3', first.TransitionGroupId)
        self.assertEqual('AAAAAAAAAAAAAAAGAGAGAK_3_b6_3', first.TransitionId)
        self.assertEqual('b6', first.Annotation)
        self.assertEqual('b', first.FragmentType)
        self.assertEqual(6, first.FragmentSeriesNumber)
        self.assertEqual(3, first.PrecursorCharge)
        self.assertEqual(3, first.ProductCharge)

        # filter
        def any_within_dist(x):
            mask1 = x['ProductMz'] >= x['PrecursorMz'] - precursor_product_limit
            mask2 = x['ProductMz'] <= x['PrecursorMz'] + precursor_product_limit
            return (mask1 & mask2).any()

        df_irt = converter.filter_and_normalise_openswath(df_irt)
        self.assertAlmostEqual(10000.0, df_irt.LibraryIntensity.max())
        self.assertGreaterEqual(df_irt.ProductMz.min(), mz_cutoff)
        self.assertFalse(any_within_dist(df_irt))

        df_library = converter.filter_and_normalise_openswath(df_library)
        self.assertAlmostEqual(10000.0, df_library.LibraryIntensity.max())
        self.assertGreaterEqual(df_library.ProductMz.min(), mz_cutoff)
        self.assertFalse(any_within_dist(df_library))

    def test_rename_modifications(self):
        converter = OpenSwathLibraryFromPeakview(
            self.hek_sciex_fname,
            modifications_to_keep='CAM,Oxi'
        )

        mods = list()
        # no mod
        mods.append(('PEPTIDE', 'PEPTIDE'))

        # valid sites for oxidation
        for site in 'M,W,H,D,K,Y,R,N,P,C'.split(','):
            mods.append((site + '[Oxi]PEPTIDE', site + '(UniMod:35)PEPTIDE'))
            mods.append((
                site + '[Oxi]PEPTIDE' + site + '[Oxi]PEPTIDE',
                site + '(UniMod:35)PEPTIDE' + site + '(UniMod:35)PEPTIDE'
            ))
        # invalid site for oxidation
        mods.append(('B[Oxi]PEPTIDE', converter.DROP_TEXT))

        # valid sites for Carbamidomethyl
        for site in 'C,D,E,H,K'.split(','):
            mods.append((site + '[CAM]PEPTIDE', site + '(UniMod:4)PEPTIDE'))
            mods.append((
                site + '[CAM]PEPTIDE' + site + '[CAM]PEPTIDE',
                site + '(UniMod:4)PEPTIDE' + site + '(UniMod:4)PEPTIDE'
            ))
        # invalid site for Carbamidomethyl
        mods.append(('B[CAM]PEPTIDE', converter.DROP_TEXT))

        # random dash
        mods.append(('M[Oxi]PEP-TIDE', converter.DROP_TEXT))

        # sites that fail
        mods.append(('B[-1K]PEPTIDE', converter.DROP_TEXT))
        mods.append(('B[-1K]PEPTIDEC[CAM]PEPTIDE', converter.DROP_TEXT))

        # keep mod peptides
        mods = pd.DataFrame(mods, columns=['input', 'expected'])
        mods['output'] = converter.rename_modifications(mods['input'])
        self.assertListEqual(
            mods['expected'].tolist(),
            mods['output'].tolist()
        )

    def test_irt_filtering(self):
        mz_cutoff, precursor_product_limit = 400, 10
        converter = OpenSwathLibraryFromPeakview(
            self.hek_sciex_fname,
            mz_cutoff=mz_cutoff,
            precursor_product_limit=precursor_product_limit,
            modifications_to_keep='CAM,Oxi'
        )
        df = converter.open_peak_view()

        _, df_irt = converter.split_to_srl_and_alignment(df)
        self.assertEqual(df_irt.shape[0], 1814, 'Expected 1814 peptides with no extra filter')
        _, df_irt = converter.split_to_srl_and_alignment(df, ['A'])
        self.assertEqual(df_irt.shape[0], 1814, "Expected 1814 peptides (no change) with filter 'A'")
        _, df_irt = converter.split_to_srl_and_alignment(df, ['GPCIGPGAGEILER', 'AQLQEPGPASGTESAHFLR'])
        self.assertEqual(df_irt.shape[0], 1828,
                         "Expected 1828 peptides (14 extra) with filter ['GPCIGPGAGEILER', 'AQLQEPGPASGTESAHFLR']")


if __name__ == '__main__':
    unittest.main()
